function [dist,inSet,proj] = SignedDist2(x,A,b)
global distances;

[noc,nos,nod] = size(A);
if length(x)~=2
    error('index should be of size two');
else
    x=x+1;
end
y=distances(x(1),x(2));
% disp(y);
x(:)=y;
if isempty(A)
    dist = inf;
    inSet = 1;
    return
end

if nod~=1  
    error('The set must be convex');
end

inSet = isPointInSet(x,A,b);

if iscell(A) % Case : union of halfspaces
    if inSet
        % if the point is in the set -> compute the complement of the set
        A_tmp = [];
        b_tmp = [];
        for i = 1:length(A)
            assert(size(A{i},1)==1)
            A_tmp = [A_tmp;-A{i}]; %#ok<AGROW>
            b_tmp = [b_tmp;-b{i}]; %#ok<AGROW>
        end
        [dist,proj] = DistFromPolyhedra(x,A_tmp,b_tmp);
    else
        %if the point is outside the set
        len_A = length(A);
        len_x = length(x);
        A_tmp = zeros(1,len_x,len_A);
        b_tmp = zeros(1,len_A);
        for i = 1:length(A)
            A_tmp(1,:,i) = A{i};
            b_tmp(i) = b{i};
        end
        [dist,proj] = DistFromPolyhedra(x,A_tmp,b_tmp);
        dist = -dist;
    end
else % Case : intersection of halfspaces
    if inSet 
        % if the point is in the set
        dist = inf;
        proj = x;
        for i = 1:noc
            tmp_min = DistFromPlane(x,A(i,:),b(i));
            if tmp_min<dist
                dist = tmp_min;
                proj = ProjOnPlane(x,A(i,:),b(i));
            end
        end
    else
        %if the point is outside the set
        [dist,proj] = DistFromPolyhedra(x,A,b);
        dist = -dist;
    end    
end

end
        

    
    