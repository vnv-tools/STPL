clc
clear all
close all

%addpath('../stql_taliro');


%squeezeFile=fopen('test_input.txt','r');
%squeezeFile=fopen('test_input_4.txt','r');
%squeezeFile=fopen('test_input_7.txt','r');
squeezeFile=fopen('squeezeDetCycle.txt','r');
sline = fgetl(squeezeFile);
Squeeze_Det={};
InputFrameSignal = [];
OBJECT_CLASSES = {'car', 'cyclist', 'pedestrian'};

FRAME_PER_SEC = 25;%25
NUM_FRAMES = 153;%153

for i=0:NUM_FRAMES
    boxes=[];
    Obj_id = 1;%object id
    Time_stamp = i*(1/FRAME_PER_SEC);
    while ischar(sline) 
        [token, remain]= strtok(sline);
        if strcmp(token,'Image')
            if i~=str2num(remain)
                str2num(remain)
                i
                error('Image number is incorrect')
            end
        else
            [token, remain]= strtok(remain);
            xmin=str2num(token);
            [token, remain]= strtok(remain);
            [token, remain]= strtok(remain);
            ymin=str2num(token);
            [token, remain]= strtok(remain);
            [token, remain]= strtok(remain);
            xmax=str2num(token);
            [token, remain]= strtok(remain);
            [token, remain]= strtok(remain);
            ymax=str2num(token);
            [token, remain]= strtok(remain);
            P=sscanf(remain(3:end-1),'%f');
            if P<=0 || P>=1
                error("Probability is incorrect");
            end
            center=[(xmin+xmax)/2,(ymin+ymax)/2];
            box=struct('type',token(1:end-1),'probability',P,'left',xmin,'top',ymin,'right',xmax,'bottom',ymax,'center',center);
            Class = find(strcmp(OBJECT_CLASSES, token(1:end-1)));
            new_row = [i, Time_stamp, Obj_id, Class, P, xmin, ymin, xmax, ymax, center(1), center(2)];
            InputFrameSignal = [InputFrameSignal; new_row];
            boxes=[boxes;box];
            Obj_id = Obj_id + 1;
        end
        sline = fgetl(squeezeFile);
        if ischar(sline)
            [token, remain]= strtok(sline);
            if strcmp(token,'Image')
                break;
            end
        end
    end
    Squeeze_Det{i+1,1}=boxes;
end
TS=[];

%Input signal structure:
%frame#, time stamp, object id, object class, class probability, x1, y1,
%x2, y2, xc, yc

%Car class = 1
%Cyclist class = 2
%Pedestrian class = 3

%@(x,f,FORALL,Var_id)
PRED_INDEX = 1;
FRAME_IDX = PRED_INDEX; PRED_INDEX = PRED_INDEX + 1; 
TIME_IDX = PRED_INDEX; PRED_INDEX = PRED_INDEX + 1; 
OBJ_ID_IDX = PRED_INDEX; PRED_INDEX = PRED_INDEX + 1; 
CLASS_IDX = PRED_INDEX; PRED_INDEX = PRED_INDEX + 1; 
PROB_IDX = PRED_INDEX; PRED_INDEX = PRED_INDEX + 1; 
X1_IDX = PRED_INDEX; PRED_INDEX = PRED_INDEX + 1; 
Y1_IDX = PRED_INDEX; PRED_INDEX = PRED_INDEX + 1; 
X2_IDX = PRED_INDEX; PRED_INDEX = PRED_INDEX + 1;
Y2_IDX = PRED_INDEX; PRED_INDEX = PRED_INDEX + 1; 
XC_IDX = PRED_INDEX; PRED_INDEX = PRED_INDEX + 1; 
YC_IDX = PRED_INDEX; 

%%@todo: this is a nested @ with two freeze variables which should be
%%syntactically unacceptable
%%STQLstr = ['@(Var_x,Var_f,EXISTS,Var_id_1) <> (cycl_1 \/ @(Var_x2,_,FORALL,Var_id_2)( spatio_1) )'];

%%%TPTLstr='[]( @ Var_x ( cycl07 -> [](  ( { Var_x>=0 }/\{ Var_x<=5 } ) -> ( cycl06 \/ ( data /\ ped06 ) ) )  ) )';

%STQLstr = ['[]( @ (Var_x,Var_f,FORALL, Var_id_1) ( (cycl_1 /\ p_cycl_1_gr_70) ->',...
%            '[](  ( {C_FRAME-Var_f>0}/\{ C_FRAME - Var_f <= 5 } ) ->',...
%            ' ( ( cycl_1 /\ p_cycl_1_gr_60) \/',... 
%            '@(EXISTS, Var_id_2)( dist_1_2_lt_40 /\',...
%            ' peds_2 /\ p_peds_2_gr_60 ) ) ) ) )'];

STQLstr = ['[]( @ (Var_x,Var_f,FORALL, Var_id_1) ( (cycl_1 /\ p_cycl_1_gr_70) ->',...
            '[](  ( {C_FRAME-Var_f >= 0}/\{ C_FRAME - Var_f <= 5 } ) ->',...
            ' @(EXISTS, Var_id_2)( (( cycl_2 /\ p_cycl_2_gr_60 ) \/',... 
            '( dist_1_2_lt_40 /\ peds_2 /\ p_peds_2_gr_60 )) ) ) ) )'];

%STQLstr = ['[]( @ (Var_x,Var_f,FORALL, Var_id_1) ( (cycl_1 /\ p_cycl_1_gr_70) ->',...
%            '[](  ( {C_FRAME-Var_f >= 0}/\{ C_FRAME - Var_f <= 5 } ) ->',...
%            ' @(EXISTS, Var_id_2)( {Var_id_1 - Var_id_2 == 0} /\ (( cycl_2 /\ p_cycl_2_gr_60 ) \/',... 
%            '( dist_1_2_lt_40 /\ peds_2 /\ p_peds_2_gr_60 )) ) ) ) )'];

PredMap = [];

ii = 1;
PredMap(ii).str = 'cycl_1';
PredMap(ii).expr = 'C(Var_id_1) == 2 ';

ii = ii+1;
PredMap(ii).str = 'p_cycl_1_gr_70';
PredMap(ii).expr = ' P(Var_id_1) > 0.70 ';

ii = ii+1;
PredMap(ii).str = 'cycl_2';
PredMap(ii).expr = 'C(Var_id_2) == 2 ';

ii = ii+1;
PredMap(ii).str = 'p_cycl_2_gr_60';
PredMap(ii).expr = 'P(Var_id_2) > 0.60';

ii = ii+1;
PredMap(ii).str = 'peds_2';
PredMap(ii).expr = 'C(Var_id_2) == 3 ';

ii = ii+1;
PredMap(ii).str = 'p_peds_2_gr_60';
PredMap(ii).expr = 'P(Var_id_2) > 0.60 ';

ii = ii+1;
PredMap(ii).str = 'dist_1_2_lt_40';
PredMap(ii).expr = 'ED(Var_id_1,CT,Var_id_2,CT) < 40 ';

START_FRAMES = 37;%37%0
END_FRAMES = 153;%120%153
INPUT_SIGNAL = InputFrameSignal;
[sig_size,sig_dym] = size(InputFrameSignal);
start_cut = 0;
end_cut = sig_size;%NUM_FRAMES;
%select a segment from the input frames
for c=1:sig_size
    if InputFrameSignal(c,1) == START_FRAMES && start_cut < 1
        start_cut = c;
    elseif InputFrameSignal(c,1) == END_FRAMES
        end_cut = c;
    end
end
if start_cut == 0
    start_cut = 1;
end
INPUT_SIGNAL = InputFrameSignal(start_cut:end_cut,:);
[sig_size,sig_dym] = size(INPUT_SIGNAL);

disp('Running...');

for i=1:1
tic
    InFrameTime = 0:1:sig_size-1;
    InFrameTime = InFrameTime';
    INPUT_SIGNAL(:,1) = INPUT_SIGNAL(:,1)-INPUT_SIGNAL(1,1);
    [rob1, aux1]=stpl_taliro(STQLstr,PredMap,INPUT_SIGNAL,InFrameTime);
    
    aux1
    rob1
    
toc
end
