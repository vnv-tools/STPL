# The STPL configurator library.
#
# This library must be compiled as a separate static library and then linked
# against because it is written in C++ and attempting to compile the sources
# directly into the executable will cause mangling issues during the process of
# linking objects.
set(STPL_CONFIGURATOR_SOURCES
  ConfigFile.cpp
  ConfigFileWrapper.cpp)

add_library(stplconfigurator STATIC ${STPL_CONFIGURATOR_SOURCES})
set_property(TARGET stplconfigurator PROPERTY POSITION_INDEPENDENT_CODE TRUE)

target_include_directories(stplconfigurator PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
install(TARGETS stplconfigurator DESTINATION lib)
