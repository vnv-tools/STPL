
#include <stdlib.h>
#include <type_traits>
#include "ConfigFile.h"
#include "ConfigFileWrapper.h"

extern "C" {
    
    FileConfig_c c_fconf;

    bool open_config_file(char* name, bool verbose) {
        FileConfig* fconf = new FileConfig(name, verbose);
        c_fconf.obj = fconf;
        c_fconf.key_size = fconf->get_size();
        return fconf->get_status();
    }

    void close_config_file() {

        ((FileConfig*)c_fconf.obj)->~FileConfig();
    }

    const char* cfg_print_config_data_map() {
        std::string str = ((FileConfig*)c_fconf.obj)->print_config_data_map().str();
        int size = (int)str.length();
        char* res = new char[size + 1];
        safe_strcpy(res , size+1, str.c_str() );
        return res;
    }

    const char* cfg_get_value(char* key) {
        std::string str = ((FileConfig*)c_fconf.obj)->get_value(key);
        int size = (int)str.length();
        char* res = new char[size + 1];
        safe_strcpy(res , size+1, str.c_str() );
        return res;
    }

    const char* cfg_get_comments(char* key) {
        std::string str = ((FileConfig*)c_fconf.obj)->get_comments(key);
        int size = (int)str.length();
        char* res = new char[size + 1];
        safe_strcpy(res , size+1, str.c_str() );
        return res;
    }

    void cfg_set_value(char* key, char* value, char* comment) {
        ((FileConfig*)c_fconf.obj)->set_value(key, value, comment);
    }

    char** cfg_get_keys() {
        std::vector<std::string> keys(((FileConfig*)c_fconf.obj)->get_keys());
        char** res;//[keys.size()];
        res = new char*[keys.size()];
        int cnt = 0;
        for(auto& key: keys){
            res[cnt] = new char[key.length()+1];
            safe_strcpy(res[cnt], key.length()+1, key.c_str());
            cnt++;
            //std::cout << "key: " << key << endl;
        }
        return res;
    }

    void cfg_update_config_file(char* name) {

    }

    int cfg_get_size(){
        c_fconf.key_size = ((FileConfig*)c_fconf.obj)->get_size();
        return c_fconf.key_size;
    }

    bool load_signal(char* name) {
        return ((FileConfig*)c_fconf.obj)->read_signal(name);
    }

    double** get_signal() {
        FileConfig* fg = (FileConfig*)c_fconf.obj;
        if (!fg->get_signal_loaded())
            return NULL;
        double** signal = NULL;
        int row = fg->get_num_sig_rows();
        int col = fg->get_num_sig_colums();
        if(((FileConfig*)c_fconf.obj)->is_verbose())
            cout << "#row: " << row << " #col: " << col << endl;
        signal = new double* [row];
        for (int i = 0; i < row; i++) {
            signal[i] = new double[col];
            std::copy((*fg->get_signal())[i].begin(), (*fg->get_signal())[i].end(), signal[i]);
            //for(int j=0; j < col; j++)
            //    cout << (*fg->get_signal()).at(i).at(j) << "\t";
            //cout << endl;
        }

        return signal;
    }

    int get_signal_rows() {
        FileConfig* fg = (FileConfig*)c_fconf.obj;
        return fg->get_num_sig_rows();
    }

    int get_signal_columns() {
        FileConfig* fg = (FileConfig*)c_fconf.obj;
        return fg->get_num_sig_colums();
    }

}
