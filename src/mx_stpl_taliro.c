/***** mx_stpl_monitor : mx_stpl_monitor.c *****/
/* Version 0.1                     */

/* Written by Mohammad Hekmatnejad ASU, U.S.A. for stpl_taliro            */
/* Copyright (c) 2020  Mohammad Hekmatnejad                                  */
/* Send bug-reports and/or questions to: mhekmatn@asu.edu                  */
/* Written by Georgios Fainekos, ASU, U.S.A. for fw_taliro                */
/* Modified by Adel Dokhanchi ASU, U.S.A. for tq_taliro                   */
/* Copyright (c) 2017  Georgios Fainekos                                  */
/* Send bug-reports and/or questions to: fainekos@gmail.com                  */

/* This program is free software; you can redistribute it and/or modify   */
/* it under the terms of the GNU General Public License as published by   */
/* the Free Software Foundation; either version 2 of the License, or      */
/* (at your option) any later version.                                    */
/*                                                                        */
/* This program is distributed in the hope that it will be useful,        */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of         */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          */
/* GNU General Public License for more details.                           */
/*                                                                        */
/* You should have received a copy of the GNU General Public License      */
/* along with this program; if not, write to the Free Software            */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA*/

/* Some of the code in this file was taken from LTL2BA software           */
/* Written by Denis Oddoux, LIAFA, France                                  */
/* Some of the code in this file was taken from the Spin software         */
/* Written by Gerard J. Holzmann, Bell Laboratories, U.S.A.               */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "undomex.h"
#include "matrix.h"
#include "distances.h"
#include "ltl2tree.h"
#include "param.h"
#include "config/ConfigFileWrapper.h"


#define BUFF_LEN 4096
int num_spatio_formulas = 0;
//double _UNIVERSE[4] = { 0,1800,0,1600 };//gloabl universe specs: min_x,max_y,min_y,max_y
double _UNIVERSE[4] = { 0,1280,0,720 };//gloabl universe specs: min_x,max_y,min_y,max_y
bool OPTIMIZE_TRJ_BASED_ON_FORMULA = false;//reduce the length of signal to 2 or one based on the temporal/spatial operators
bool MAGNIFY_ROB_4_NEG_LHS_IMPLY = false;//forces the robust value of the LHS of IMPLIES to be ignored
int UX_START = -1;
int UY_START = -1;
int U_WIDTH = -1;
int U_HEIGHT = -1;
bool read_global_variables_from_config_file(char* input_file_name, bool verbose_from_command);

//----------------------------
//CONFIG FILE VARIABLES
bool DEBUG_MODE = false;
bool VERBOSE_MODE = false;
bool SHOW_EXE_STAT = false;
bool SANITY_CHECK = false;
bool BOOLEAN_ROB = false;
bool RELEASE_DP_TABLE_MEMORY = false;
bool RELEASE_SPATIAL_MEMORY = false;
bool SHOW_WARNINGS = false;
unsigned int STQL_SIGNAL_DIM_SIZE = 11;
unsigned int MAX_STQL_FORMULA_SIZE = 1000;
unsigned int MAX_MAP_DATA_SIZE = 100;

/// Flagged if API function is used.
///
/// This option is set to silence any output produced during the monitoring
/// process. If output is preferred through APIs, set to false.
bool API = false;

/* forward declarations */
mxArray* nonmexFunction(char* stpl_formula, int size_formula, int num_predicates,
			char* pred_str[], char* pred_expr[],
			double* input_signal, int num_rows, int num_columns,
			double* TStamps);

/* PUBLIC API */

/// Evaluate the robustness of a perception datastream signal.
///
/// This is the public interface for calling the STPL monitor through external
/// languages and programs. It should be noted that while the monitor provides
/// various configurations, the default set are currently used.
///
/// Furthermore, this interface uses boolean semantics only in the evaluation to
/// reduce additional data conversions.
///
/// TODO: Support continuous evaluation semantics.
///
/// @param formula An STPL requirement.
/// @param formula_size The number of characters (bytes) of the `formula`.
/// @param num_preds The number of predicates in `formula`.
/// @param pred_names The list of predicate names found in the `formula`.
/// @param pred_exprs The list of predicate expressions (i.e., STPL subformulas).
/// @param signal The perception datastream signal as contiguous array.
/// @param nrows The number of rows in the `signal`.
/// @param ncols The number of columns in the `signal`.
/// @param timestamps The list of timestamps corresponding to the signal.
/// @param frame_width The width of the video frame.
/// @param frame_height The height of the video frame.
///
/// @return The signal satisfies the formula.
bool monitor(
    char* formula,
    int formula_size,
    int num_preds,
    char **pred_names,
    char **pred_exprs,
    double* signal,
    int nrows,
    int ncols,
    double* timestamps,
    double frame_width,
    double frame_height
) {
  API = true; // silence monitoring output

  // These settings are concretely set as the other option is to use an external
  // configuration file. However, other libraries/tools may not want to use this
  // configuration scheme, so this should be eventually adjustable through
  // this interface.

  BOOLEAN_ROB = true; // REQUIRED
  DEBUG_MODE = false;
  MAGNIFY_ROB_4_NEG_LHS_IMPLY = false;
  OPTIMIZE_TRJ_BASED_ON_FORMULA = false;
  RELEASE_DP_TABLE_MEMORY = false;
  RELEASE_SPATIAL_MEMORY = false;
  SANITY_CHECK = false;
  SHOW_EXE_STAT = false;
  SHOW_WARNINGS = false;
  VERBOSE_MODE = false;

  MAX_STQL_FORMULA_SIZE = 1000;
  MAX_MAP_DATA_SIZE = 100;
  UX_START = 0;
  UY_START = 0;

  // The signal dimension corresponding to data.
  // format: <frame><time><id><class><prob><xmin><ymin><xmax><ymax><xcenter><ycenter>
  STQL_SIGNAL_DIM_SIZE = 11;

  // Because the datastream can vary, these properties of the input must be
  // provided by when interfacing with the monitor.
  U_WIDTH = frame_width;
  U_HEIGHT = frame_height;

  // monitoring
  mxArray* rob = nonmexFunction(formula, formula_size, num_preds, pred_names,
				pred_exprs, signal, nrows, ncols, timestamps);

  return mxGetLogicals(mxGetField(rob, 0, "ds"))[0];
}

void report_error_exit(const char* msg) {
    printf(msg);
    exit(0);
}


char* stringFromConstKW(enum SpatioConstKW kw)
{
    //    CONST_UNKNOWN = 0, CONST_CT, CONST_LM, CONST_RM, CONST_TM, CONST_BM, CONST_EMPTYSET, CONST_UNIVERSE
    static const char* strings[] = { "UNKNOWN_KW", "CT", "LM", "RM", "TM" , "BM" , "EMPTYSET" , "UNIVERSE" };

    return (char*)strings[kw];
}

int indexFromConstStringKW(char* key) {
    for (int i = 0; i < NUM_SPATIO_CONST; i++) {
        if (strcmp(stringFromConstKW(i), key) == 0)
            return i;
    }
    return -1;
}

char * emalloc(size_t n)
{   
    if (n == 0) {
        if(SHOW_WARNINGS)
            mexPrintf("WARNING: memory allocation with size 0\n");
        return NULL;//0
    }
    char *tmp;
    if (!(tmp = (char*)malloc(n))) {
        //if (!(tmp = (char*)mxMalloc(n)))
        mexErrMsgTxt("mx_stpl_monitor: not enough memory!");
        return NULL;
    }
    memset(tmp, 0, n);
    return tmp;
}

void efree(void* tmp) {
    free(tmp);
    //mxFree(tmp);
    tmp = NULL;
}

int tl_Getchar(int *cnt, size_t hasuform, char *uform)
{
    if (*cnt < hasuform)
        return uform[(*cnt)++];
    (*cnt)++;
    return -1;
}

void tl_UnGetchar(int *cnt)
{
    if (*cnt > 0) (*cnt)--;
}

#define Binop(a)        \
        fprintf(miscell->tl_out, "(");    \
        dump(n->lft, miscell);        \
        fprintf(miscell->tl_out, a);    \
        dump(n->rgt, miscell);        \
        fprintf(miscell->tl_out, ")")

static void sdump(Node *n, char    *dumpbuf)
{
    switch (n->ntyp) {
    case PREDICATE:    safe_strcat(dumpbuf, MAX_VAR_NAME_LENGTH, n->sym->name);
            break;
    case U_OPER:    safe_strcat(dumpbuf, MAX_VAR_NAME_LENGTH, "U");
            goto common2;
    case V_OPER:    safe_strcat(dumpbuf, MAX_VAR_NAME_LENGTH, "V");
            goto common2;
    case OR:    safe_strcat(dumpbuf, MAX_VAR_NAME_LENGTH, "|");
            goto common2;
    case AND:    safe_strcat(dumpbuf, MAX_VAR_NAME_LENGTH, "&");
common2:        sdump(n->rgt,dumpbuf);
common1:        sdump(n->lft,dumpbuf);
            break;
    case NEXT:    safe_strcat(dumpbuf, MAX_VAR_NAME_LENGTH, "X");
            goto common1;
    case WEAKNEXT:    safe_strcat(dumpbuf, MAX_VAR_NAME_LENGTH, "W");
            goto common1;
    case PREVIOUS:    safe_strcat(dumpbuf, MAX_VAR_NAME_LENGTH, "P");
            goto common1;
    case WEAKPREVIOUS:    safe_strcat(dumpbuf, MAX_VAR_NAME_LENGTH, "Z");
            goto common1;
    case NOT:    safe_strcat(dumpbuf, MAX_VAR_NAME_LENGTH, "!");
            goto common1;
    case TRUE:    safe_strcat(dumpbuf, MAX_VAR_NAME_LENGTH, "T");
            break;
    case FALSE:    safe_strcat(dumpbuf, MAX_VAR_NAME_LENGTH, "F");
            break;
    default:    safe_strcat(dumpbuf, MAX_VAR_NAME_LENGTH, "?");
            break;
    }
}

Symbol *DoDump(Node *n, char *dumpbuf, Miscellaneous *miscell)
{
    if (!n) return ZS;

    if (n->ntyp == PREDICATE)
        return n->sym;

    dumpbuf[0] = '\0';
    sdump(n,dumpbuf);
    return tl_lookup(dumpbuf, miscell);
}

void dump(Node *n, Miscellaneous *miscell)
{
    if (!n) return;

    switch(n->ntyp) {
    case OR:    Binop(" || "); break;
    case AND:    Binop(" && "); break;
    case U_OPER:    Binop(" U ");  break;
    case V_OPER:    Binop(" V ");  break;
    case NEXT:
        fprintf(miscell->tl_out, "X");
        fprintf(miscell->tl_out, " (");
        dump(n->lft, miscell);
        fprintf(miscell->tl_out, ")");
        break;
    case WEAKNEXT:
        fprintf(miscell->tl_out, "W");
        fprintf(miscell->tl_out, " (");
        dump(n->lft, miscell);
        fprintf(miscell->tl_out, ")");
        break;
    case PREVIOUS:
        fprintf(miscell->tl_out, "P");
        fprintf(miscell->tl_out, " (");
        dump(n->lft, miscell);
        fprintf(miscell->tl_out, ")");
        break;
    case WEAKPREVIOUS:
        fprintf(miscell->tl_out, "Z");
        fprintf(miscell->tl_out, " (");
        dump(n->lft, miscell);
        fprintf(miscell->tl_out, ")");
        break;
    case NOT:
        fprintf(miscell->tl_out, "!");
        fprintf(miscell->tl_out, " (");
        dump(n->lft, miscell);
        fprintf(miscell->tl_out, ")");
        break;
    case FALSE:
        fprintf(miscell->tl_out, "false");
        break;
    case TRUE:
        fprintf(miscell->tl_out, "true");
        break;
    case PREDICATE:
        fprintf(miscell->tl_out, "(%s)", n->sym->name);
        break;
    case -1:
        fprintf(miscell->tl_out, " D ");
        break;
    default:
        printf("Unknown token: ");
        tl_explain(n->ntyp);
        break;
    }
}

void tl_explain(int n)
{
    switch (n) {
    case ALWAYS:    printf("[]"); break;
    case EVENTUALLY: printf("<>"); break;
    case IMPLIES:    printf("->"); break;
    case EQUIV:    printf("<->"); break;
    case PREDICATE:    printf("predicate"); break;
    case OR:    printf("||"); break;
    case AND:    printf("&&"); break;
    case NOT:    printf("!"); break;
    case U_OPER:    printf("U"); break;
    case V_OPER:    printf("V"); break;
    case NEXT:    printf("X"); break;
    case WEAKNEXT:    printf("W"); break;
    case PREVIOUS:    printf("P"); break;
    case WEAKPREVIOUS:    printf("Z"); break;
    case TRUE:    printf("true"); break;
    case FALSE:    printf("false"); break;
    case ';':    printf("end of formula"); break;
    default:    printf("%c", n); break;
    }
}

static void non_fatal(char *s1, char *s2, int *cnt, char *uform, int *tl_yychar, Miscellaneous *miscell)
{
    int i;

    printf("STPL_Monitor: ");
    if (s2)
        printf(s1, s2);
    else
        printf(s1);
    if ((*tl_yychar) != -1 && (*tl_yychar) != 0)
    {    printf(", saw '");
        tl_explain((*tl_yychar));
        printf("'");
    }
    printf("\nSTPL_Monitor: %s\n---------", uform);
    for (i = 0; i < (*cnt); i++)
        printf("-");
    printf("^\n");
    fflush(stdout);
    (miscell->tl_errs)++;
}

void tl_yyerror(char *s1, int *cnt, char *uform, int *tl_yychar, Miscellaneous *miscell)
{
    Fatal(s1, (char *) 0, cnt, uform, tl_yychar, miscell);
}

//note: capital F in Fatal
void Fatal(char *s1, char *s2, int *cnt, char *uform, int *tl_yychar, Miscellaneous *miscell)
{
  non_fatal(s1, s2, cnt, uform, tl_yychar, miscell);
  tl_exit(0);
}

void fatal(char *s1, char *s2, int *cnt, char *uform, int *tl_yychar, Miscellaneous *miscell)
{
        non_fatal(s1, s2, cnt, uform, tl_yychar, miscell);
        tl_exit(0);
}

void put_uform(char *uform, Miscellaneous *miscell)
{
    fprintf(miscell->tl_out, "%s", uform);
}

void tl_exit(int i)
{
    mexErrMsgTxt("mx_stpl_monitor: unexpected error, tl_exit executed.");
}

/* used for visualizing STQL formula tree */
void print2file(int *c, Node *n, FILE * f){
    int i;
    if (!n) return;
    (*c)++;
    i=(*c);
    switch (n->ntyp)
    {
    case TRUE:
        fprintf(f, "  \"TRUE(%d)\"\n",i);
        break;
    case FALSE:
        fprintf(f, "  \"FALSE(%d)\"\n",i);
        break;
    case PREDICATE:
        fprintf(f, "  \"%s(%d)\"\n", n->sym->name,i);
        break;
    case NOT:
        fprintf(f, "  \"NOT(%d)\"\n",i);
        fprintf(f, "  \"NOT(%d)\" ->",i);
        print2file(c,n->lft,f);
        break;
    case AND:
        fprintf(f, "  \"AND(%d)\"\n",i);
        fprintf(f, "  \"AND(%d)\" ->",i);
        print2file(c, n->lft, f);
        fprintf(f, "  \"AND(%d)\" ->",i);
        print2file(c, n->rgt, f);
        break;
    case OR:
        fprintf(f, "  \"OR(%d)\"\n",i);
        fprintf(f, "  \"OR(%d)\" ->",i);
        print2file(c, n->lft, f);
        fprintf(f, "  \"OR(%d)\" ->",i);
        print2file(c, n->rgt, f);
        break;
    case IMPLIES:
        fprintf(f, "  \"IMPLIES(%d)\"\n",i);
        fprintf(f, "  \"IMPLIES(%d)\" ->",i);
        print2file(c, n->lft, f);
        fprintf(f, "  \"IMPLIES(%d)\" ->",i);
        print2file(c, n->rgt, f);
        break;
    case NEXT:
        fprintf(f, "  \"NEXT(%d)\"\n",i);
        fprintf(f, "  \"NEXT(%d)\" ->",i);
        print2file(c, n->lft, f);
        break;
    case WEAKNEXT:
        fprintf(f, "  \"WEAKNEXT(%d)\"\n",i);
        fprintf(f, "  \"WEAKNEXT(%d)\" ->",i);
        print2file(c, n->lft, f);
        break;
    case PREVIOUS:
        fprintf(f, "  \"PREVIOUS(%d)\"\n",i);
        fprintf(f, "  \"PREVIOUS(%d)\" ->",i);
        print2file(c, n->lft, f);
        break;
    case WEAKPREVIOUS:
        fprintf(f, "  \"WEAKPREVIOUS(%d)\"\n",i);
        fprintf(f, "  \"WEAKPREVIOUS(%d)\" ->",i);
        print2file(c, n->lft, f);
        break;
    case U_OPER:
        fprintf(f, "  \"U(%d)\"\n",i);
        fprintf(f, "  \"U(%d)\" ->",i);
        print2file(c, n->lft, f);
        fprintf(f, "  \"U(%d)\" ->",i);
        print2file(c, n->rgt, f);
        break;
    case V_OPER:
        fprintf(f, "  \"R(%d)\"\n",i);
        fprintf(f, "  \"R(%d)\" ->",i);
        print2file(c, n->lft, f);
        fprintf(f, "  \"R(%d)\" ->",i);
        print2file(c, n->rgt, f);
        break;
    case EVENTUALLY:
        fprintf(f, "  \"<>(%d)\"\n",i);
        fprintf(f, "  \"<>(%d)\" ->",i);
        print2file(c, n->rgt, f);
        break;
    case ALWAYS:
        fprintf(f, "  \"[](%d)\"\n",i);
        fprintf(f, "  \"[](%d)\" ->",i);
        print2file(c, n->rgt, f);
        break;
    case FREEZE_AT:
        fprintf(f, "  \"@ %s(%d)\"\n", n->sym->name,i);
        fprintf(f, "  \"@ %s(%d)\" ->", n->sym->name,i);
        print2file(c, n->lft, f);
        break;
    case CONSTR_LE:
        fprintf(f, "  \"%s <= %f (index=%d)\"\n", n->sym->name, n->value.numf.f_num,i);
        break;
    case CONSTR_LS:
        fprintf(f, "  \"%s < %f (index=%d)\"\n", n->sym->name, n->value.numf.f_num,i);
        break;
    case CONSTR_EQ:
        fprintf(f, "  \"%s == %f (index=%d)\"\n", n->sym->name, n->value.numf.f_num,i);
        break;
    case CONSTR_GE:
        fprintf(f, "  \"%s >= %f (index=%d)\"\n", n->sym->name, n->value.numf.f_num,i);
        break;
    case CONSTR_GR:
        fprintf(f, "  \"%s > %f (index=%d)\"\n", n->sym->name, n->value.numf.f_num,i);
        break;
    default:
        break;
    }
}


//c: returns the number of freeze time/frame variables 
//cnt_qtf: returns the number of quantifier operators 
void countVar(int *c, Node *n, char * varName, int g, int* cnt_qtf){

    if (!n) return;

    n->group=g;

    switch (n->ntyp)
    {
    case FREEZE_AT://note: for @(EXISTS/FORALL,...) there is no symbol predicate
        if (n->sym != NULL) {
            //this is @(Var_x,...)
            varName = n->sym->name;
            (*c)++;//increases the number of time/frame variables
            n->group = (*c);
            if(n->fndef != NULL && n->fndef->num_param > 2 )
                (*cnt_qtf)++;//increases the number of quantifier operators
            countVar(c, n->lft, varName, n->group, cnt_qtf);
        }
        else if (n->fndef != NULL) {//this is @(EXISTS/FORALL,Var_id...)
            (*cnt_qtf)++;//increases the number of quantifier operators
            countVar(c, n->lft, varName, g, cnt_qtf);
        }
        else
            countVar(c, n->lft, varName, g, cnt_qtf);
        break;
    case CONSTR_LE:
    case CONSTR_LS:
    case CONSTR_EQ:
    case CONSTR_GE:
    case CONSTR_GR:
        break;
    case AND:
    case OR:
    case IMPLIES:
    case U_OPER:
    case V_OPER:
        countVar(c, n->lft, varName, g, cnt_qtf);
        countVar(c, n->rgt, varName, g, cnt_qtf);
        break;
    case NEXT:
    case WEAKNEXT:
    case PREVIOUS:
    case WEAKPREVIOUS:
    case NOT:
        countVar(c, n->lft, varName, g, cnt_qtf);
        break;
    case EVENTUALLY:
    case ALWAYS:
        countVar(c, n->rgt, varName, g, cnt_qtf);
        break;
    case FALSE:
    case TRUE:
        n->group = g;
        break;
    default:
        break;
    }
}

void print_fn_def(FnDef fn) {
    if (fn.num_param == 0) {
        mexPrintf("%s", fn.fn_name);
        return;
    }
    mexPrintf("%s(", fn.fn_name);
    for (int i = 0; i < fn.num_param; i++) {
        print_fn_def(fn.params[i]);
        if (i != (fn.num_param - 1))
            mexPrintf(",");
    }
    mexPrintf(")");
    return;
}

void print_opr(int opr) {
    switch (opr)
    {
    case GE_OPR:
        mexPrintf(">=");
        break;
    case LE_OPR:
        mexPrintf("<=");
        break;
    case G_OPR:
        mexPrintf(">");
        break;
    case L_OPR:
        mexPrintf("<");
        break;
    case EQ_OPR:
        mexPrintf("==");
        break;
    case NE_OPR:
        mexPrintf("!=");
        break;
    default:
        break;
    }
}

void print_const_opr(int opr) {
    switch (opr)
    {
    case CONSTR_GE:
        mexPrintf(">=");
        break;
    case CONSTR_LE:
        mexPrintf("<=");
        break;
    case CONSTR_GR:
        mexPrintf(">");
        break;
    case CONSTR_LS:
        mexPrintf("<");
        break;
    case CONSTR_EQ:
        mexPrintf("==");
        break;
    case CONSTR_NE:
        mexPrintf("!=");
        break;
    default:
        break;
    }
}

void show_error_on_fn_expr(int cnt, char* expr) {
    //mexPrintf(" An expected function expression could be simillar to: @(param_1,param_2) FUNCTION_NAME(param_1) {>,<,>=,<=,=} FUNCTION_NAME(param_2)");
    //mexPrintf(", or: @(param_1) FUNCTION_NAME(param_1) {>,<,>=,<=,=} NUMBER\n");
    mexPrintf(" An expected function expression could be simillar to the below examples: \n");
    mexPrintf("FUNCTION_NAME(param_1) { > , < , >= , <= , ==, != } FUNCTION_NAME(param_2, param_3)\n");
    mexPrintf("FUNCTION_NAME(param_1) {>, <, >=, <=, ==, !=} NUMBER\n");
    mexPrintf("%s\n",expr);
    for (int i = 0; i < cnt-1; i++)
        mexPrintf("-");
    mexPrintf("^\n");
    mexErrMsgTxt("mx_stpl_monitor: error in function expression syntax.");
}

bool is_valid_name_char(char c) {
    if (c >= 'A' && c <= 'Z')
        return true;
    if (c >= 'a' && c <= 'z')
        return true;
    if (c >= '0' && c <= '9')
        return true;
    if (c == '_')
        return true;
    return false;
}

int add_unique_var_in_map_spatio(struct Map* map, Node* node, int* index, FnExpr* fn_expr) {
    if (node == NULL)
        return (*index);
    FnDef* fn = node->fndef;
    if (fn != NULL) {
        if (fn->num_param == 0) {
            int fim = find_in_map(map, fn->fn_name, (*index));
            if ( fim < 0 &&
                strncmp("Var_", fn->fn_name, 4) == 0)
            {
                if (*index > MAX_UNIQUE_VAR_ID_IN_PRED) {
                    print_fn_def(fn_expr->left_fn);
                    print_opr(fn_expr->opr);
                    print_fn_def(fn_expr->right_fn);
                    mexPrintf("Error in the above spatial formula.");
                    mexErrMsgTxt("Violation in maximum number of Var_id in a spatial formula.");
                }
                add_map(map, fn->fn_name, 1, index);
            }
            return (*index);
        }

        for (int i = 0; i < fn->num_param; i++) {

            add_unique_var_in_map(map, &fn->params[i], index, fn_expr);
            int fim = find_in_map(id_to_rank_map, fn->params[i].fn_name, size_id_to_rank_map);
            if (fn->params[i].fn_type == VAR_UNKNOWN &&
                fim >= 0)
                fn->params[i].fn_type = VAR_ID;
        }
        return (*index);
    }
    add_unique_var_in_map_spatio(map, node->lft, index, fn_expr);
    add_unique_var_in_map_spatio(map, node->rgt, index, fn_expr);
    return (*index);
}

int add_unique_var_in_map(struct Map* map, FnDef* fn, int * index, FnExpr* fn_expr) {
    if (fn->num_param == 0) {
        if (find_in_map(map, fn->fn_name, (*index)) < 0 &&
            strncmp("Var_",fn->fn_name,4) == 0) 
        {
            if (*index > MAX_UNIQUE_VAR_ID_IN_PRED) {
                print_fn_def(fn_expr->left_fn);
                print_opr(fn_expr->opr);
                print_fn_def(fn_expr->right_fn);
                mexPrintf("Error in the above function expression.");
                mexErrMsgTxt("Violation in maximum number of Var_id in a function expression.");
            }
            add_map(map, fn->fn_name, 1, index);
        }
        else if (fn->fn_type == VAR_SPATIO) {
            add_unique_var_in_map_spatio(map, fn->spatio_formula, index, fn_expr);
        }
        return (*index);
    }

    for (int i = 0; i < fn->num_param; i++) {

        add_unique_var_in_map(map, &fn->params[i], index, fn_expr);
        if(fn->params[i].fn_type == VAR_UNKNOWN && 
                find_in_map(id_to_rank_map, fn->params[i].fn_name, size_id_to_rank_map) >= 0)
            fn->params[i].fn_type = VAR_ID;
    }
    return (*index);
}

int count_num_unique_vars_in_pred_expression(FnExpr* fn_expr) {
    if (fn_expr == NULL)
        mexErrMsgTxt("NULL pointer passed to count_num_unique_vars_in_pred_expression");
    struct Map var_id_map[MAX_UNIQUE_VAR_ID_IN_PRED];//maximum two unique Var_ variables are allowed in each predicate expression
    int map_size = 0;
    int unique_num = 0;
    int left_unique_num = 0;
    left_unique_num = add_unique_var_in_map(var_id_map, &fn_expr->left_fn, &unique_num, fn_expr);
    int right_unique_num = 0;
    right_unique_num = add_unique_var_in_map(var_id_map, &fn_expr->right_fn, &unique_num, fn_expr);
    return unique_num;
}

int find_close_pair(char* expr, int start, char open_symbol, char close_symbol) {
    
    int end = (int)strlen(expr);
    int found_open = 0;
    bool first_opened = false;
    
    for (int i = start; i < end; i++) {
        
        if (expr[i] == open_symbol) {
            found_open++;
            first_opened = true;
        }
        else if (expr[i] == close_symbol)
            found_open--;

        if (found_open == 0 && first_opened)
            return i;//the index of closed pair
    }
    return -1;//not found
}

//Note: this is a recursive function that parse nested function expressions
//i.e., F1(VAR_x, F2(F3(VAR_y), 3), F4 ( VAR_x , Var_z ) )
FnDef parse_fn_expression(FnExpr* fnExpr, char* expression, int start, int end, bool can_be_const) {
    static int fn_expr_index = 0;//gloabl counter to assign unique index to each function definition
    char* expr = expression + start;
    int expr_len = end - start;
    char c;
    int cnt = 0;
    #define MAX_NUM_PARAM  20
    #define MAX_NAME_LENGTH 100
    int num_param;
    int hasuform = expr_len;
    int rcnt = 0;
    char fn_name[MAX_NAME_LENGTH];
    FnDef fn_param_def[MAX_NUM_PARAM];
    FnDef res_expr;

    do {
        c = tl_Getchar(&cnt, expr_len, expr);
    } while (c == ' ');

    num_param = 0;
    res_expr.num_param = 0;
    res_expr.findex = fn_expr_index++;
    res_expr.num_vars = 0;

    do {//repeat for all the parameters delimitered by ','

        if (c == ',') {
            do {
                c = tl_Getchar(&cnt, expr_len, expr);
            } while (c == ' ');
        }

        //check if this is a number
        rcnt = 0;
        
        //this is a spatio formula
        if (c == '{') {
            //static int spatio_formula_cnt = 0;
            int close_index = find_close_pair(expr, cnt - 1, '{', '}');
            if (close_index >= 0) {
                int new_len = close_index - cnt;
                char* tmp = (char*)emalloc((close_index - cnt + 1) * sizeof(char));
                safe_strncpy(tmp, close_index - cnt + 1, expression + start + cnt, new_len);
                tmp[new_len] = '\0';

                //fn_param_def[num_param].spatio_formula = (Node*)
                //    parse_spatio_formula(expression, start + cnt, start + new_len);//<<<<<<<<<<<
                fn_param_def[num_param].spatio_formula = (Node*)
                    parse_spatio_formula(tmp, 0, new_len);//<<<<<<<<<<<

                efree(tmp);

                safe_strcpy(fn_name, MAX_NAME_LENGTH, "SPATIO-FORMULA-\0");
                char buf[10];

                safe_sprintf(buf, 10, "%d", num_spatio_formulas++);
                safe_strcat(fn_name, MAX_NAME_LENGTH, buf);
                //safe_itoa(num_spatio_formulas++, buf, 10, 10);
                //safe_sprintf(buf, 10, "%d", num_spatio_formulas++);
                //safe_strcat(fn_name, MAX_NAME_LENGTH, buf);
                fn_param_def[num_param].fn_name = (char*)emalloc((strlen(fn_name)+1) * sizeof(char));
                safe_strcpy(fn_param_def[num_param].fn_name, strlen(fn_name)+1, fn_name);
                fn_param_def[num_param].num_param = 0;
                fn_param_def[num_param].num_vars = 0;
                fn_param_def[num_param].fn_type = VAR_SPATIO;
                res_expr.fn_name = (char*)emalloc((strlen(fn_name)+1) * sizeof(char));
                safe_strcpy(res_expr.fn_name, strlen(fn_name)+1, fn_name);
                num_param++;
                res_expr.num_param++;
                fnExpr->num_spatio_formula++;

            }
            else {
                mexPrintf("Spatio formula open curly bracket do not match closed ones.");
                show_error_on_fn_expr(cnt, expr);
            }
        }
        else if ((c < 'A' || c > 'Z') && !can_be_const) {
            mexPrintf("Function/Variable name must start with a capital letter.\n");
            show_error_on_fn_expr(cnt, expr);
        }
        else if (c >= '0' && c <= '9') {//check if this is a constant number

            do {
                fn_name[rcnt++] = c;
                c = tl_Getchar(&cnt, expr_len, expr);
            } while ((c >= '0' && c <= '9') || c == '.');

            fn_name[rcnt] = '\0';
            res_expr.fn_name = (char*)emalloc((rcnt+1) * sizeof(char));
            safe_strcpy(res_expr.fn_name, rcnt+1, fn_name);
            fn_param_def[num_param].fn_type = VAR_CONST;
            fn_param_def[num_param].fn_name = (char*)emalloc((rcnt+1) * sizeof(char));
            fn_param_def[num_param].num_param = 0;
            safe_strcpy(fn_param_def[num_param].fn_name, rcnt+1, fn_name);
            fn_param_def[num_param].num_vars = 0;
            num_param++;
            res_expr.num_param++;
        }
        //this is a function or a variable
        else {
            do {
                fn_name[rcnt++] = c;
                c = tl_Getchar(&cnt, expr_len, expr);
            } while (is_valid_name_char(c));

            fn_name[rcnt] = '\0';

            res_expr.fn_name = (char*)emalloc((rcnt+1) * sizeof(char));
            safe_strcpy(res_expr.fn_name, rcnt+1, fn_name);

            while (c == ' ') {
                c = tl_Getchar(&cnt, expr_len, expr);
            }

            if (c != '(' && c != ',' && cnt <= expr_len) {
                mexPrintf("Unexpected function expression!");
                show_error_on_fn_expr(cnt, expr);
            }
            else if (cnt == (expr_len + 1) || c == ',') {//this is an unknown type variable 
                fn_param_def[num_param].fn_name = (char*)emalloc((rcnt+1) * sizeof(char));
                safe_strcpy(fn_param_def[num_param].fn_name, rcnt+1, fn_name);
                fn_param_def[num_param].num_param = 0;
                fn_param_def[num_param].fn_type = VAR_UNKNOWN;

                for (int i = 0; i < NUM_SPATIO_CONST; i++) {
                    if(strcmp(fn_name, stringFromConstKW(i)) == 0)
                        fn_param_def[num_param].fn_type = VAR_RESERVED;
                }
                
                if (strncmp("Var_", fn_name, 4) == 0) {
                    fn_param_def[num_param].num_vars = 1;

                    //@todo: *** comment for now ***
                    //we fill out type_map in the tl_parse which is called after calls to this function
                    if (find_in_map(type_map, fn_name, size_type_map) < 0) {
                    //    mexPrintf("%s is not defined in any freez definitions.\n", fn_name);
                    //    mexErrMsgTxt("Syntax Error! An unknown variable is used in the predicate definitions.\n");
                    }
                }
                else
                    fn_param_def[num_param].num_vars = 0;

                num_param++;
                res_expr.num_param++;
            }
            else if (c == '(') {//this is a nested function

                int fn_index = -1;
                //check if this is a valid function name
                for (int i = 0; i < NUM_FN_NAMES; i++) {
                    if (strcmp(fn_name, FN_FAMES[i]) == 0) {
                        fn_index = i;
                        break;
                    }
                }
                if (fn_index < 0) {
                    mexPrintf("'%s' is not a valid function name.\n", fn_name);
                    mexErrMsgTxt("The function is not recognized.");
                }

                int close_index = find_close_pair(expr, cnt - 1, '(', ')');
                if (close_index >= 0) {
                    int new_len = close_index - cnt;
                    fn_param_def[num_param] = parse_fn_expression(fnExpr, expression, 
                                                        start + cnt, start + close_index, true);
                    fn_param_def[num_param].fn_name = (char*)emalloc((rcnt+1) * sizeof(char));
                    safe_strcpy(fn_param_def[num_param].fn_name, rcnt+1, fn_name);
                    
                    do {
                        c = tl_Getchar(&cnt, expr_len, expr);
                    } while (c == ' ');
                        fn_param_def[num_param].fn_type = VAR_FUN;
                    num_param++;
                    cnt = close_index + 2;
                    if (cnt < expr_len)
                        c = expr[cnt - 1];
                    else
                        c = '\0';
                }
                else {
                    mexPrintf("Function open paranthesis do not match closed ones.");
                    show_error_on_fn_expr(cnt, expr);
                }
            }
        }
        while (c == ' ')
            c = tl_Getchar(&cnt, expr_len, expr);

    } while (c == ',');

    if (num_param == 1 && start == 0)//this is the root function
        return fn_param_def[0];


    res_expr.num_param = num_param;
    res_expr.params = (FnDef*)emalloc(num_param * sizeof(FnDef));
    int total_var_num = 0;
    for (int i = 0; i < num_param; i++) {
        res_expr.params[i] = fn_param_def[i];
        if (fn_param_def[i].fn_type != VAR_SPATIO)
            fn_param_def[i].spatio_formula = NULL;
        total_var_num += fn_param_def[i].num_vars;
    }
    res_expr.num_vars = total_var_num;
    return res_expr;
}

FnExpr* parse_fn_predicate_expression(char* expr) {
    /*{
    //what to expect in an example//
    //char* test_expr = "F1(VAR_x, F2(F3(VAR_y), 3), F4 ( VAR_x , Var_z ) )\0";
    //char* test_expr = "F1(VAR_x, 3, Var_z  )\0";
    //char* test_expr = "F1(VAR_x, F2(3,4), Var_z  )\0";
    //char* test_expr = "Var_x\0";
    //char* test_expr = "345\0";
    //FnDef res = parse_fn_expression(test_expr, 0, strlen(test_expr), true);
    //print_fn_def(res);
    //mexPrintf("\nDONE.\n");
    }*/
    
    #define MAX_NUM_PARAM  20
    #define MAX_NAME_LENGTH 10000

    FnExpr* res_expr;
    res_expr = (FnExpr*)emalloc(sizeof(FnExpr));
    static int *cnt = 0;
    int tmp_cnt = 0;
    int expr_len = (int)strlen(expr);
    char c;
    char left_fn[MAX_NAME_LENGTH];
    char right_fn[MAX_NAME_LENGTH];
    bool left_has_spatial_formula = false;
    bool right_has_spatial_formula = false;
    cnt = &tmp_cnt;
    res_expr->num_spatio_formula = 0;
    //left hand side
    do {
        c = tl_Getchar(cnt, expr_len, expr);
    } while (c == ' ');
    if (c < 'A' || c > 'Z') {
        mexPrintf("Left hand side of a predicate must start with a Function/Variable.");
        show_error_on_fn_expr(*cnt, expr);
    }
    //left hand side must be a function/variable
    int lcnt = 0;
    do {
        if (lcnt >= MAX_NAME_LENGTH)
            mexErrMsgTxt("parse_fn_predicate_expression: Too large formula!");
        left_fn[lcnt++] = c;
        c = tl_Getchar(cnt, expr_len, expr);
        //check if this is a <> 'Eventually' operator not a comparison operator
        //this is only a case for the left hand side for which we look for
        //a comparison operator such as > < >= <= == !=
        if (c == '{') {
            left_has_spatial_formula = true;
            int close_index = find_close_pair(expr, *cnt - 1, '{', '}');
            if (close_index >= 0) {
                int new_len = close_index - *cnt;
                if ((lcnt + new_len) >= MAX_NAME_LENGTH) {
                    mexErrMsgTxt("parse_fn_predicate_expression: Too large formula!");
                    show_error_on_fn_expr(*cnt, expr);
                }

                safe_strncpy(left_fn + lcnt, MAX_NAME_LENGTH-lcnt, expr + *cnt -1, new_len + 1);
                *cnt += new_len + 1;
                lcnt += new_len + 1;
                c = '}';
            }
            else {
                mexPrintf("Spatio formula open curly bracket do not match closed ones.");
                show_error_on_fn_expr(*cnt, expr);
            }
        }
    } while (c!='<' && c!='>' && c!='=' && c!='!');

    if (lcnt >= MAX_NAME_LENGTH)
        mexErrMsgTxt("parse_fn_predicate_expression: Too large formula!");

    left_fn[lcnt] = '\0';

    res_expr->left_fn = parse_fn_expression(res_expr, left_fn, 0, lcnt, false);

    //comparasion operator
    switch (c) {
        case '>':
            c = tl_Getchar(cnt, expr_len, expr);
            if (c == '=')
                res_expr->opr = GE_OPR;
            else
                res_expr->opr = G_OPR;
        break;
        case '<':
            c = tl_Getchar(cnt, expr_len, expr);
            if (c == '=')
                res_expr->opr = LE_OPR;
            else
                res_expr->opr = L_OPR;
        break;
        case '=':
            c = tl_Getchar(cnt, expr_len, expr);
            if (c == '=')
                res_expr->opr = EQ_OPR;
            else {
                mexPrintf("A comparison operator is expexted (>,<,>=,<=,==,!=).");
                show_error_on_fn_expr(*cnt, expr);
            }
            break;
        case '!':
            c = tl_Getchar(cnt, expr_len, expr);
            if (c == '=')
                res_expr->opr = NE_OPR;
            else{
                mexPrintf("A comparison operator is expexted (>,<,>=,<=,==,!=).");
                show_error_on_fn_expr(*cnt, expr);
            }
            break;
        default:
            mexPrintf("A comparison operator is expexted (>,<,>=,<=,==,!=).");
            show_error_on_fn_expr(*cnt, expr);
            break;
    }

    //right hand side
    do {
        c = tl_Getchar(cnt, expr_len, expr);
    } while (c == ' ');
    //right hand side is a number/function/variable
    int rcnt = 0;
    do {
        if (c == '{') {
            right_has_spatial_formula = true;
            int close_index = find_close_pair(expr, *cnt - 1, '{', '}');
            if (close_index <= 0) {
                mexPrintf("Spatio formula open curly bracket do not match closed ones.");
                show_error_on_fn_expr(*cnt, expr);
            }

        }
        right_fn[rcnt++] = c;
        c = tl_Getchar(cnt, expr_len, expr);
    } while (c>0);
    right_fn[rcnt] = '\0';

    res_expr->right_fn = parse_fn_expression(res_expr, right_fn, 0, rcnt, true);
    res_expr->num_unique_vars = count_num_unique_vars_in_pred_expression(res_expr);

    if (DEBUG_MODE) 
    {
        mexPrintf("\nPredicate expression:\n");
        print_fn_def(res_expr->left_fn);
        print_opr(res_expr->opr);
        print_fn_def(res_expr->right_fn);
        mexPrintf("\n");
        mexPrintf("number of unique variables: %d\n", res_expr->num_unique_vars);
    }
    return res_expr;
}

void release_fn_expression(FnDef* fn) {
    if (fn == NULL)
        return;
    if (fn->num_param > 0) {
        if (fn->params != NULL) {
            for (int i = 0; i < fn->num_param; i++)
                release_fn_expression(&fn->params[i]);
            efree(fn->params);
        }
    }
    if(DEBUG_MODE)
        mexPrintf("%s deleted.\n", fn->fn_name);
    efree(fn->fn_name);
}

void release_fn_expr_from_nodes(Node* root) {
    if (root == NULL)
        return;
    else if (root->fndef != NULL)
        release_fn_expression(root->fndef);

    release_fn_expr_from_nodes(root->lft);
    release_fn_expr_from_nodes(root->rgt);
}
#if 1
void run_spatial_test1() {
    SpatialRegion* reg = createSpatialRegion(1);
    SpatialNode* node = createSpatialNode(1);
    reg->head = node;
    reg->size = 1;
    node->bbox[0] = 3;
    node->bbox[1] = 7;
    node->bbox[2] = 4;
    node->bbox[3] = 10;
    node->itv[0] = 1;
    node->itv[1] = 1;
    node->itv[2] = 1;
    node->itv[3] = 1;
    node->conv_set = NULL;
    node->next = NULL_SN;
    node->has_bbox = true;
    for (int i = 0; i < 100; i++) {
        SpatialRegion* tmp = createSpatialRegion(1);
        tmp->head = createSpatialNode(1);
        tmp->size = 1;
        tmp->head->conv_set = NULL;
        tmp->head->has_bbox = true;
        tmp->head->next = NULL_SN;
        for (int j = 0; j < 4; j++) {
            tmp->head->bbox[j] = node->bbox[j];
            tmp->head->itv[j] = node->itv[j];
        }
        spatio_complement(tmp, reg);
        print_spatial_region(tmp);
        mexCallMATLAB(0, NULL, 0, NULL, "drawnow");//to flush printouts in Matlab
        //release_spatial_node(&tmp->head);
        //hfree(tmp);
        release_spatial_region(&tmp);

        SpatialRegion* r1 = createSpatialRegion(1);
        SpatialRegion* r2 = createSpatialRegion(1);
        SpatialRegion* res = createSpatialRegion(1);
        s_copyToFrom(r1, reg);
        s_copyToFrom(r2, reg);
        spatio_complement(res, r1);
        print_spatial_region(res);
        //release_spatial_node(&res->head);
        //hfree(res);
        release_spatial_region(&res);
        res = createSpatialRegion(1);
        spatio_complement(res, r2);
        print_spatial_region(res);
        //release_spatial_node(&r1->head);
        //release_spatial_node(&r2->head);
        //release_spatial_node(&res->head);
        release_spatial_region(&r1);
        release_spatial_region(&r2);
        release_spatial_region(&res);
    }
    release_spatial_region(&reg);
    //mexErrMsgTxt("TEST 1 ENDED!");
    mexPrintf("***TEST 1 ENDED!***\n");
}

SpatialRegion* createRegion(int x1, int x2, int y1, int y2, int b1, int b2, int b3, int b4) {
    SpatialRegion* reg = createSpatialRegion(1);
    SpatialNode* node = createSpatialNode(1);
    reg->head = node;
    reg->size = 1;
    node->bbox[0] = x1;
    node->bbox[1] = x2;
    node->bbox[2] = y1;
    node->bbox[3] = y2;
    node->itv[0] = b1;
    node->itv[1] = b2;
    node->itv[2] = b3;
    node->itv[3] = b4;
    node->conv_set = NULL;
    node->next = NULL_SN;
    node->has_bbox = true;
    reg->is_NAN = false;
    return reg;
}

#define reg_num 8
void run_spatial_test2() {
    SpatialRegion* reg[reg_num];//

    reg[0] = createRegion(0, 20, 0, 10, 1, 1, 1, 1);
    reg[1] = createRegion(20, 30, 0, 20, 1, 1, 1, 1);
    reg[2] = createRegion(0, 30, 10, 40, 1, 1, 1, 1);
    reg[3] = createRegion(0, 10, 10, 40, 1, 1, 1, 1);
    reg[4] = createRegion(10, 30, 10, 40, 1, 1, 1, 1);
    reg[5] = createRegion(0, 20, 30, 50, 1, 1, 1, 1);
    reg[6] = createRegion(0, 30, 40, 50, 1, 1, 1, 1);
    reg[7] = createRegion(0, 30, 10, 50, 1, 1, 1, 1);

    SpatialRegion* r1 = createSpatialRegion(1);
    SpatialRegion* r2 = createSpatialRegion(1);
    SpatialRegion* res = createSpatialRegion(1);
    SpatialRegion* tmp = createSpatialRegion(1);

    for (int i = 0; i < reg_num; i++) {

        s_copyToFrom(r1, reg[i]);
        s_copyToFrom(r2, reg[i]);
        mexPrintf("Region: \n");
        print_spatial_region(r1);
        spatio_complement(res, r1);
        mexPrintf("Compelment: \n");
        print_spatial_region(res);
        spatio_complement(tmp, res);
        mexPrintf("Complement of Complement: \n");
        print_spatial_region(tmp);
        if (equalRegions(r1, tmp))
            mexPrintf("test passed Ok.\n");
        else
            mexPrintf("test Failed!\n");
    }
    
    //release_spatial_node(&r1->head);
    //release_spatial_node(&r2->head);
    //release_spatial_node(&res->head);
    //hfree(r1);
    //hfree(r2);
    //hfree(res);
    release_spatial_region(&r1);
    release_spatial_region(&r2);
    release_spatial_region(&res);

    res = createSpatialRegion(1);
    r1 = createSpatialRegion(1);
    r2 = createSpatialRegion(1);
    for (int i = 0; i < reg_num; i++) {
        s_copyToFrom(r1, reg[i]);
        mexPrintf("Region1: \n");
        print_spatial_region(r1);
        mexPrintf("Region2: \n");
        print_spatial_region(res);
        spatio_union(r2, res, r1);
        mexPrintf("Union: \n");
        print_spatial_region(r2);
        s_copyToFrom(res, r2);
        mexPrintf("Union cloned: \n");
        print_spatial_region(res);
    }

    mexPrintf("\n\n============== Intersection ================\n\n");

    for (int i = 0; i < reg_num; i++) {
        s_copyToFrom(r1, reg[i]);
        mexPrintf("Region1: \n");
        print_spatial_region(r1);
        mexPrintf("All Union Region: \n");
        print_spatial_region(res);
        spatio_intersection(r2, res, r1);
        mexPrintf("Intersection: \n");
        print_spatial_region(r2);
        if (equalRegions(r1, r2))
            mexPrintf("test passed Ok.\n");
        else
            mexPrintf("test Failed!\n");
    }
    release_spatial_region(&tmp);
    for(int i =0 ; i < reg_num; i++)
        release_spatial_region(&reg[i]);

    //mexErrMsgTxt("TEST ENDED!");
    mexPrintf("***TEST 2 ENDED!***\n");
}

void run_spatial_test3() {

    const int h_size = 1;
    const int w_size = 1;
    const int w_skip = 1;
    const int h_skip = 1;

    int Uw = (int)(_UNIVERSE[1] - _UNIVERSE[0]);
    int Uh = (int)(_UNIVERSE[3] - _UNIVERSE[2]);
    int cnt = 0;

    should_release_memory = false;
    SpatialRegion* main_sr = NULL_SR;
    SpatialRegion* tmp_sr;

    for (int i = 0; i < Uw; i += w_skip) {
        for (int j = 0; j < Uh; j += h_skip) {
            if (main_sr == NULL_SR) {
                main_sr = createRegion(i, i + w_size, j, j + h_size, 1, 1, 1, 1);
                tmp_sr = main_sr;
            }
            else {
                tmp_sr->next = createRegion(i, i + w_size, j, j + h_size, 1, 1, 1, 1);
                tmp_sr = tmp_sr->next;
            }
            cnt++;
        }
    }
    mexPrintf("%d Spatial Region created.\n", cnt);

    cnt = 0;
    while (main_sr != NULL_SR) {
        tmp_sr = main_sr;
        main_sr = main_sr->next;
        release_spatial_region(&tmp_sr); 
        cnt++;
    }

    mexPrintf("%d Spatial Region deleted.\n", cnt);


    cnt = 0;
    for (int c = 0; c < 20; c++) {
        for (int i = 0; i < Uw; i += w_skip) {
            for (int j = 0; j < Uh; j += h_skip) {
                if (main_sr == NULL_SR) {
                    main_sr = createRegion(i, i + w_size, j, j + h_size, 1, 1, 1, 1);
                    tmp_sr = main_sr;
                }
                else {
                    tmp_sr->next = createRegion(i, i + w_size, j, j + h_size, 1, 1, 1, 1);
                    tmp_sr = tmp_sr->next;
                }
                cnt++;
            }
        }
    }
    mexPrintf("%d Spatial Region created.\n", cnt);

    cnt = 0;
    while (main_sr != NULL_SR) {
        tmp_sr = main_sr;
        main_sr = main_sr->next;
        release_spatial_region(&tmp_sr);
        cnt++;
    }

    mexPrintf("%d Spatial Region deleted.\n", cnt);



    should_release_memory = true;

    //mexErrMsgTxt("TEST ENDED!");
    mexPrintf("***TEST 3 ENDED!***\n");
}

#endif

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{    
    run_from_matlab = true;

    bool is_open = read_global_variables_from_config_file("stpl.conf", false);
    if (is_open)
        mexPrintf("Reading from config file stpl.conf to load some parameters.\n");
    else
        mexPrintf("Warning: Could not read/find stpl.conf\n");

    /* Variables needed to process the input */
    int status;// , pstatus;
    mwSize buflen;// , pbuflen;
    size_t NElems,NCells;
    mwSize ndimA, ndimb, ndimG, ndim, ndimP;// , pdim;
    const mwSize* dimsA, * dimsb, * dimsG, * dims, * dimsP;// , * pardims;
    mwIndex jstruct, iii, jjj, i1, j1, idx_j;
    mxArray *tmp,*tmp1,*tmp_cell;    
    /* Variables needed for monitor */
    Node *node;
    double *XTrace = NULL, *TStamps= NULL, *LTrace = NULL;
    DistCompData distData;
    PMap *predMap; 
    int ii,jj,kk,ll,objs,tempI;
    bool par_on;
    bool fn_expr_on = false;
    bool is_Multi_HA;
    bool initial_of_par;
    int npred, npar;
    int nfnexpr = 0;

    static char    uform[BUFF_LEN];
    static size_t hasuform=0;
    static int *cnt;
    int temp = 0;


    Miscellaneous *miscell = (Miscellaneous *) emalloc(sizeof(Miscellaneous));
    int *tl_yychar = (int *) emalloc(sizeof(int));
    miscell->dp_taliro_param.LTL = 1; 
    miscell->dp_taliro_param.ConOnSamples = 0; 
    miscell->dp_taliro_param.SysDim = 0; 
    miscell->dp_taliro_param.nSamp = 0; 
    miscell->dp_taliro_param.nPred = 0; 
    miscell->dp_taliro_param.true_nPred = 0; 
    miscell->dp_taliro_param.tnLoc = 0; 
    miscell->dp_taliro_param.nInp = 0; 
    miscell->dp_taliro_param.nCLG = 1;
    miscell->tl_errs = 0;
    miscell->type_temp = 0;

    /* Reset cnt to 0:
        cnt is the counter that points to the next symbol in the formula
        to be processed. This is a static variable and it retains its 
        value between Matlab calls to mx_stpl_monitor. */
    cnt = &temp;

    /* Other initializations */
    miscell->dp_taliro_param.nInp = nrhs;
    par_on = false;
    initial_of_par = false;
    npred = 0;
    npar= 0;
    bool has_predicate_map = true;
 
    /* Make sure the I/O are in the right form */
    if(nrhs < 3)
        mexErrMsgTxt("mx_stpl_monitor: At least 3 inputs are required.");
    else if(nlhs > 1)
      mexErrMsgTxt("mx_stpl_monitor: Too many output arguments.");
    else if(nrhs > 8)
      mexErrMsgTxt("mx_stpl_monitor: Too many input arguments.");
    else if(!mxIsChar(prhs[0]))
      mexErrMsgTxt("mx_stpl_monitor: 1st input must be a string with TL formula.");
    else if (!mxIsStruct(prhs[1])) {
        //mexErrMsgTxt("mx_stpl_monitor: 2nd input must be a structure (predicate map).");
        mexPrintf("WARNING: mx_stpl_monitor: 2nd input must be a structure (predicate map).\n");
        has_predicate_map = false;
    }
    else if(!mxIsDouble(prhs[2]))
      mexErrMsgTxt("mx_stpl_monitor: 3rd input must be a numerical array (State trace).");
    else if(nrhs>3 && !mxIsDouble(prhs[3]))
      mexErrMsgTxt("mx_stpl_monitor: 4th input must be a numerical array (Time stamps).");
    else if(nrhs>5 && !mxIsDouble(prhs[4]))
      mexErrMsgTxt("mx_stpl_monitor: 5th input must be a numerical array (Location trace).");
    else if(nrhs>5 && !(mxIsDouble(prhs[5])||mxIsCell(prhs[5])))
      mexErrMsgTxt("mx_stpl_monitor: 6th input must be a numerical array \n (Minimum path distance to each control location for each predicate).");
    else if(nrhs>7 && !(mxIsCell(prhs[6])))
      mexErrMsgTxt("mx_stpl_monitor: 7th input must be a cell array (Adjacency list).");
    else if(nrhs>7 && !(mxIsStruct(prhs[7])||mxIsCell(prhs[7])))
      mexErrMsgTxt("mx_stpl_monitor: 8th input must be a structure (guard map).");

    if(nlhs > 1)
        mexErrMsgTxt("Too many output arguments.");
    plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);

    /* Process inputs */
    /* Get the formula */
    ndim = mxGetNumberOfDimensions(prhs[0]);
    dims = mxGetDimensions(prhs[0]);
    buflen = (dims[1] +1) * sizeof(char);
    if (buflen >= BUFF_LEN)
    {
      mexPrintf("%s%d%s\n", "The formula must be less than ", BUFF_LEN," characters.");
      mexErrMsgTxt("mx_stpl_monitor: Formula too long.");
    }
    status = mxGetString(prhs[0], uform, buflen);   
    hasuform = strlen(uform);
    for (iii=0; iii<hasuform; iii++)
    {
        if (uform[iii] == '\t' || uform[iii] == '\"' || uform[iii] == '\n')                        
            uform[iii] = ' ';
    }

    /* Get state trace */        
    ndim = mxGetNumberOfDimensions(prhs[2]);
    if (ndim>2)
        mexErrMsgTxt("mx_stpl_monitor: The state trace is not in proper form!"); 
    dims = mxGetDimensions(prhs[2]);
    if (dims[0] < 1)
        mexErrMsgTxt("STPL: Input signal cannot be empty.");
    miscell->dp_taliro_param.nSamp = dims[0];
    miscell->dp_taliro_param.SysDim = dims[1];
    XTrace = mxGetPr(prhs[2]);
    
        //check if the input signal is in a right format w.r.t STQL
    bool stql_compliance = true;
    if(dims[1] < STQL_SIGNAL_DIM_SIZE)
        stql_compliance = false;
    else {
        int j = dims[0];
        for (int i = 0; i < dims[0] - 1; i++) {
            if (XTrace[i] != (int)fabs(XTrace[i]) ||//frame numbers are integer i.e., 0,1,2...,n
                XTrace[i + 1] < XTrace[i] ||//frame numbers are monotically increasing
                XTrace[j] != fabs(XTrace[j]) ||//time stamps are positive
                XTrace[j + 1] < XTrace[j] //time stamps are monotically increasing
                ) {
                stql_compliance = false;
                mexPrintf("f=%f t=%f\n", XTrace[i], XTrace[i + 1]);
                break;
            }
            j++;
        }
    }
    if (!stql_compliance)
        if(SHOW_WARNINGS)
            mexPrintf("WARNING: the input signal is not in compliance with STPL!\n");
    if (stql_compliance) {
                miscell->dp_taliro_param.nFrame = (int)(XTrace[dims[0] - 1] - XTrace[0] + 1);
    }
    else {
        miscell->dp_taliro_param.nFrame = 0;
    }

    /* Get time stamps */       
    if (nrhs>3)
    {
        ndim = mxGetNumberOfDimensions(prhs[3]);
        if (ndim>2)
            mexErrMsgTxt("mx_stpl_monitor: The time stamp sequence is not in proper form!"); 
        dims = mxGetDimensions(prhs[3]);
        if (miscell->dp_taliro_param.nSamp != dims[0])
            mexErrMsgTxt("mx_stpl_monitor: The lengths of the time stamp sequence and the state trace do not match!"); 
        TStamps = mxGetPr(prhs[3]);
    }

    /* Get location trace and location graph */       
    if (nrhs>4)
    {
        ndim = mxGetNumberOfDimensions(prhs[4]);
        if (ndim>2)
            mexErrMsgTxt("mx_stpl_monitor: The location trace is not in proper form!"); 
        dims = mxGetDimensions(prhs[4]);
        if (miscell->dp_taliro_param.nSamp != dims[0])
            mexErrMsgTxt("mx_stpl_monitor: The lengths of the location trace and the state trace do not match!"); 
        LTrace = mxGetPr(prhs[4]);

        /* For Multiple H.A.s is added  */
        miscell->dp_taliro_param.nCLG = dims[1];
        if(nrhs>5 && (mxIsCell(prhs[5]))){
            is_Multi_HA=1;
                    miscell->dp_taliro_param.nCLG = dims[1];
            ndim = mxGetNumberOfDimensions(prhs[5]);
            if (ndim>2)
            {
                mexErrMsgTxt("wrong dimension"); 
            }
            dims = mxGetDimensions(prhs[5]);
            if (dims[0]!=1)
            {
                mexErrMsgTxt("DMin is a cell, it must be a column vector cell!"); 
            }
            if (dims[1]!=miscell->dp_taliro_param.nCLG)
            {
                mexErrMsgTxt("DMin is a cell, it must be of the size of nCLG!"); 
            }
            distData.LDistNCLG = (double **)emalloc((miscell->dp_taliro_param.nCLG)*sizeof(double*));
            miscell->dp_taliro_param.tnLocNCLG = (mwSize*)emalloc((miscell->dp_taliro_param.nCLG)*sizeof(mwSize));
            for(kk=0; kk<miscell->dp_taliro_param.nCLG; kk++){
                tmp = mxGetCell(prhs[5],kk);
                dims = mxGetDimensions(tmp);
                miscell->dp_taliro_param.tnLocNCLG[kk] = dims[0];
                distData.LDistNCLG[kk]=mxGetPr(tmp);
            }

//                 mexErrMsgTxt("mx_stpl_monitor: Multiple Hybrid Automata is not suppoterd in TQ_TaLiRo!"); 
        }
        else if(nrhs>5) {
            is_Multi_HA=0;
            ndim = mxGetNumberOfDimensions(prhs[5]);
            if (ndim>2)
                mexErrMsgTxt("mx_stpl_monitor: The minimum distance array is not in proper form!"); 
            dims = mxGetDimensions(prhs[5]);
            miscell->dp_taliro_param.tnLoc = dims[0];
            distData.LDist = mxGetPr(prhs[5]);
        }
         /*  For Multiple H.A.s */
    }

    /* Get guards */
    if (nrhs>7 && is_Multi_HA==0)
    {   /*  ONE Hybrid Automata  */                  
        NElems = mxGetNumberOfElements(prhs[6]);
        if (NElems==0)
        {
            mexErrMsgTxt("mx_stpl_monitor: the adjacency list is empty!");
        }
        distData.AdjL = (double **)emalloc(NElems*sizeof(double*));
        distData.AdjLNell = (size_t *)emalloc(NElems*sizeof(size_t));
        for (ii=0; ii<NElems; ii++)
        {
            distData.AdjL[ii] = mxGetPr(mxGetCell(prhs[6],ii));
            ndim = mxGetNumberOfDimensions(mxGetCell(prhs[6],ii));
            dims = mxGetDimensions(mxGetCell(prhs[6],ii));
            if (ndim>2 || dims[0]>1)
            {
                mexErrMsgTxt("mx_stpl_monitor: The adjacency list is not in correct format!"); 
            }
            distData.AdjLNell[ii] = dims[1];
        }

        ndimG = mxGetNumberOfDimensions(prhs[7]);
        if (ndimG>2)
        {
            mexErrMsgTxt("mx_stpl_monitor: The guard sets are not in proper form!"); 
        }
        dimsG = mxGetDimensions(prhs[7]);
        if ((dimsG[0] != dimsG[1]) || (dimsG[0] != miscell->dp_taliro_param.tnLoc))
        {
            mexErrMsgTxt("mx_stpl_monitor: The guard array must be a square array structure or \n the dimensions of the guard array do not match the adjacency matrix!"); 
        }
        distData.GuardMap = (GuardSet **)emalloc(dimsG[0]*sizeof(GuardSet*));
        for (ii=0; ii<dimsG[0]; ii++)
        {
            if (distData.AdjLNell[ii]>0)
            {
                distData.GuardMap[ii] = (GuardSet *)emalloc(distData.AdjLNell[ii]*sizeof(GuardSet));
                for (jj=0; jj<distData.AdjLNell[ii]; jj++)
                {
                    /* Get set for guard (ii,jj) */
                    idx_j = ((mwIndex) distData.AdjL[ii][jj])-1;
                    /* for projection is added */
                    tmp_cell = mxGetField(prhs[7], ii+idx_j*dimsG[0], "proj");
                    if(tmp_cell == NULL)
                       {
                        distData.GuardMap[ii][jj].nproj=0;
                    }
                    else
                    {
                        ndimP = mxGetNumberOfDimensions(tmp_cell);
                        if (ndimP>2)
                        {
                            mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                            mexErrMsgTxt("mx_stpl_monitor: Above guard: The projection map is not in proper form!"); 
                        }
                        dimsP = mxGetDimensions(tmp_cell);
                        if(dimsP[0]!=1)
                        {
                            mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                            mexErrMsgTxt("mx_stpl_monitor: Above guard: The projection map must be a row vector !"); 
                        }
                        if(dimsP[1]>miscell->dp_taliro_param.SysDim)
                        {
                            mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                            mexErrMsgTxt("mx_stpl_monitor: Above guard: The projection map must have size less than or equal to the system dimentions !"); 
                        }
                        distData.GuardMap[ii][jj].nproj=dimsP[1];
                        distData.GuardMap[ii][jj].proj=(int *)emalloc(distData.GuardMap[ii][jj].nproj*sizeof(int));
                        tempI=0;
                        for (iii=0; iii<distData.GuardMap[ii][jj].nproj; iii++)
                        {
                            distData.GuardMap[ii][jj].proj[iii] = (int)(mxGetPr(tmp_cell))[iii];
                            if(distData.GuardMap[ii][jj].proj[iii]>miscell->dp_taliro_param.SysDim)
                            {
                                mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                                mexErrMsgTxt("mx_stpl_monitor: Above guard: The projection index must be less than or equal to the system dimentions !"); 
                            }
                            if(distData.GuardMap[ii][jj].proj[iii]<=tempI)
                            {
                                mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                                mexErrMsgTxt("mx_stpl_monitor: Above guard: The projection index must be incremental !"); 
                            }
                            else
                            {
                                tempI=distData.GuardMap[ii][jj].proj[iii];
                            }
                        }
                    }
                    /* for projection */
                    idx_j = ((mwIndex) distData.AdjL[ii][jj])-1;
                    tmp_cell = mxGetField(prhs[7], ii+idx_j*dimsG[0], "A");
                    if (tmp_cell == NULL)
                    {
                        mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                        mexErrMsgTxt("mx_stpl_monitor: Above guard: Field 'A' is undefined!"); 
                    }
                    /* If it is a cell, then the guard set is a union of polytopes */
                    if (mxIsCell(tmp_cell))
                    {
                        ndim = mxGetNumberOfDimensions(tmp_cell);
                        if (ndim>2)
                        {
                            mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                            mexErrMsgTxt("mx_stpl_monitor: Above guard: if A is a cell, it must be a column vector cell!"); 
                        }
                        dims = mxGetDimensions(tmp_cell);
                        if (dims[0]!=1)
                        {
                            mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                            mexErrMsgTxt("mx_stpl_monitor: Above guard: if A is a cell, it must be a column vector cell!"); 
                        }
                        distData.GuardMap[ii][jj].nset = dims[1]; /* the number of sets */
                    }
                    else
                    {
                        /* For backward combatibility, non-cell inputs should be also accepted */
                        distData.GuardMap[ii][jj].nset = 1; /* the number of sets */
                    }
                    
                    distData.GuardMap[ii][jj].ncon = (int *)emalloc(distData.GuardMap[ii][jj].nset*sizeof(int));
                    distData.GuardMap[ii][jj].nproj=0;
                    distData.GuardMap[ii][jj].A = (double ***)emalloc(distData.GuardMap[ii][jj].nset*sizeof(double**));
                    for (kk=0; kk<distData.GuardMap[ii][jj].nset; kk++)
                    {
                        if (mxIsCell(tmp_cell))
                        {
                            tmp = mxGetCell(tmp_cell,kk);
                        }
                        else
                        {
                            tmp = tmp_cell;
                        }
                        ndimA = mxGetNumberOfDimensions(tmp);
                        if (ndimA>2)
                        {
                            mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                            mexErrMsgTxt("mx_stpl_monitor: Above guard: A is not in proper form!"); 
                        }
                        dimsA = mxGetDimensions(tmp);
                        if (miscell->dp_taliro_param.SysDim != dimsA[1])
                        {
                            mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                            mexErrMsgTxt("mx_stpl_monitor: Above guard: The dimensions of the set constraints and the state trace do not match!"); 
                        }
                        distData.GuardMap[ii][jj].ncon[kk] = dimsA[0]; /* the number of constraints */
                        if (distData.GuardMap[ii][jj].ncon[kk]>2 && miscell->dp_taliro_param.SysDim==1)
                        {
                            mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                            mexErrMsgTxt("mx_stpl_monitor: Above guard: For 1D signals only up to two constraints per predicate are allowed!\n More than two are redundant!"); 
                        }
                        distData.GuardMap[ii][jj].A[kk] = (double **)emalloc(distData.GuardMap[ii][jj].ncon[kk]*sizeof(double*));
                        for (i1=0; i1<distData.GuardMap[ii][jj].ncon[kk]; i1++)
                        {
                            distData.GuardMap[ii][jj].A[kk][i1] = (double *)emalloc(miscell->dp_taliro_param.SysDim*sizeof(double));
                            for (j1=0; j1<miscell->dp_taliro_param.SysDim; j1++)
                            {
                                distData.GuardMap[ii][jj].A[kk][i1][j1] = (mxGetPr(tmp))[i1+j1*distData.GuardMap[ii][jj].ncon[kk]];
                        
                            }
                        }
                    }
                    /* get b */
                    tmp_cell = mxGetField(prhs[7], ii+idx_j*dimsG[0], "b");
                    if (tmp_cell == NULL)
                    {
                        mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                        mexErrMsgTxt("mx_stpl_monitor: Above guard: Field 'b' is undefined!"); 
                    }
                    /* If it is a cell, then the guard set is a union of polytopes */
                    if (mxIsCell(tmp_cell))
                    {
                        ndim = mxGetNumberOfDimensions(tmp_cell);
                        if (ndim>2)
                        {
                            mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                            mexErrMsgTxt("mx_stpl_monitor: Above guard: if b is a cell, it must be a column vector cell!"); 
                        }
                        dims = mxGetDimensions(tmp_cell);
                        if (dims[0]!=1)
                        {
                            mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                            mexErrMsgTxt("mx_stpl_monitor: Above guard: if b is a cell, it must be a column vector cell!"); 
                        }
                        if (distData.GuardMap[ii][jj].nset!=dims[1])
                        {
                            mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                            mexErrMsgTxt("mx_stpl_monitor: Above guard: the dimensions of 'A' and 'b' must match!"); 
                        }
                    }
                    distData.GuardMap[ii][jj].b = (double **)emalloc(distData.GuardMap[ii][jj].nset*sizeof(double*));
                    for (kk=0; kk<distData.GuardMap[ii][jj].nset; kk++)
                    {
                        if (mxIsCell(tmp_cell))
                        {
                            tmp = mxGetCell(tmp_cell,kk);
                        }
                        else
                        {
                            tmp = tmp_cell;
                        }
                        ndimb = mxGetNumberOfDimensions(tmp);
                        if (ndimb>2)
                        {
                            mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                            mexErrMsgTxt("mx_stpl_monitor: Above guard: The set constraints are not in proper form!"); 
                        }
                        dimsb = mxGetDimensions(tmp);
                        if (distData.GuardMap[ii][jj].ncon[kk] != dimsb[0])
                        {
                            mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                            mexErrMsgTxt("mx_stpl_monitor: Above guard: The number of constraints between A and b do not match!"); 
                        }
                        distData.GuardMap[ii][jj].b[kk] = mxGetPr(tmp);
                        if (distData.GuardMap[ii][jj].ncon[kk]==2 && miscell->dp_taliro_param.SysDim==1)
                        {
                            if ((distData.GuardMap[ii][jj].A[kk][0][0]>0 && distData.GuardMap[ii][jj].A[kk][1][0]>0) || 
                                (distData.GuardMap[ii][jj].A[kk][0][0]<0 && distData.GuardMap[ii][jj].A[kk][1][0]<0))
                            {
                                mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                                mexErrMsgTxt("mx_stpl_monitor: Above guard: The set has redundant constraints! Please remove redundant constraints."); 
                            }
                            if (!((distData.GuardMap[ii][jj].A[kk][0][0]<0 && (distData.GuardMap[ii][jj].b[kk][0]/distData.GuardMap[ii][jj].A[kk][0][0]<=distData.GuardMap[ii][jj].b[kk][1]/distData.GuardMap[ii][jj].A[kk][1][0])) || 
                                (distData.GuardMap[ii][jj].A[kk][1][0]<0 && (distData.GuardMap[ii][jj].b[kk][1]/distData.GuardMap[ii][jj].A[kk][1][0]<=distData.GuardMap[ii][jj].b[kk][0]/distData.GuardMap[ii][jj].A[kk][0][0]))))
                            {
                                mexPrintf("%s%d%s%d%s \n", "Guard (",ii+1,",",idx_j+1,")");
                                mexErrMsgTxt("mx_stpl_monitor: Above guard: The set is empty! Please modify the constraints."); 
                            }
                        }
                    }
                }
            }
        }
       /*  ONE Hybrid Automata  */        
    }

    /* Get predicate map*/
    NElems = mxGetNumberOfElements(prhs[1]);
    miscell->dp_taliro_param.nPred = NElems;
    miscell->dp_taliro_param.true_nPred = NElems;
    if (NElems==0 && has_predicate_map)
        mexErrMsgTxt("mx_stpl_monitor: the predicate map is empty!");
    predMap = (PMap *)emalloc((NElems+MAX_PREDICATE_EXPRESSION_IN_FORMULA)*sizeof(PMap));
    //miscell->parMap = (ParMap *)emalloc((NElems + MAX_PREDICATE_EXPRESSION_IN_FORMULA) *sizeof(ParMap));
    miscell->predMap = (PMap *)emalloc((NElems + MAX_PREDICATE_EXPRESSION_IN_FORMULA) *sizeof(PMap));

    miscell->pList.pindex=(int *)emalloc((NElems + MAX_PREDICATE_EXPRESSION_IN_FORMULA) *sizeof(int));


    /* initial predicate list*/
    for(ll = 0; ll < NElems; ll++)
    {
        miscell->pList.pindex[ll] = -1;
    }

    for(jstruct = 0; jstruct < NElems; jstruct++) 
    {
        fn_expr_on = false;
        predMap[jstruct].is_expression = false;

        /* Get predicate name */
        tmp = mxGetField(prhs[1], jstruct, "str");
        if(tmp == NULL)
        {
            tmp = mxGetField(prhs[1], jstruct, "par");
            if(tmp == NULL) 
            {
                mexPrintf("%s%d\n", "Predicate no ", jstruct+1);
                mexErrMsgTxt("mx_stpl_monitor: The above parameter must has either the 'str' field or 'par' field!");
            }
            else
            {    
                par_on = true;
                npar++;
            }
        }
        else
        {
            par_on = false;
            npred++;
        }

        tmp = mxGetField(prhs[1], jstruct, "expr");
        if (tmp != NULL)
        {
            fn_expr_on = true;
            nfnexpr++;
        }


        /* If this is a parameter */
        if(par_on)
        {
            mexErrMsgTxt("mx_stpl_monitor: parameter estimation is not supported by STPL-Monitor !"); 
        }
                else if (fn_expr_on) {//<<<<<<<<
            /* Get name of the predicate */
            tmp = mxGetField(prhs[1], jstruct, "str");
            ndim = mxGetNumberOfDimensions(tmp);
            dims = mxGetDimensions(tmp);
            buflen = (dims[1] +1) * sizeof(char);
            predMap[jstruct].str = (char*)emalloc(buflen);
            miscell->predMap[jstruct].str = (char*)emalloc(buflen);
            predMap[jstruct].set.idx = (int)jstruct;
            predMap[jstruct].true_pred = true;
            status = mxGetString(tmp, predMap[jstruct].str, buflen);
            status = mxGetString(tmp, miscell->predMap[jstruct].str, buflen);

            /* Get function expression*/
            tmp = mxGetField(prhs[1], jstruct, "expr");
            ndim = mxGetNumberOfDimensions(tmp);
            dims = mxGetDimensions(tmp);
            buflen = (dims[1] +1) * sizeof(char);
            predMap[jstruct].expr = (char*)emalloc(buflen);
            miscell->predMap[jstruct].expr = (char*)emalloc(buflen);
            predMap[jstruct].is_expression = true;
            miscell->predMap[jstruct].is_expression = true;
            status = mxGetString(tmp, predMap[jstruct].expr, buflen);
            status = mxGetString(tmp, miscell->predMap[jstruct].expr, buflen);
            predMap[jstruct].fn_expr = parse_fn_predicate_expression(predMap[jstruct].expr);//<<<<<<<<<
            if (VERBOSE_MODE)
            {
                mexPrintf("\nPredicate expression:\n%s ::= ", predMap[jstruct].str);
                print_fn_def(predMap[jstruct].fn_expr->left_fn);
                print_opr(predMap[jstruct].fn_expr->opr);
                print_fn_def(predMap[jstruct].fn_expr->right_fn);
                mexPrintf("\n");
                mexPrintf("number of unique variables: %d\n", predMap[jstruct].fn_expr->num_unique_vars);
            }

            //miscell->predMap[jstruct].fn_expr = predMap[jstruct].fn_expr;
        }
        else
        {
            /* Get name of the predicate */
            tmp = mxGetField(prhs[1], jstruct, "str");
            ndim = mxGetNumberOfDimensions(tmp);
            dims = mxGetDimensions(tmp);
            buflen = (dims[1]+1) * sizeof(char);
            predMap[jstruct].str = (char *)emalloc(buflen); 
            miscell->predMap[jstruct].str = (char *)emalloc(buflen); 
            predMap[jstruct].set.idx = (int) jstruct;   
            predMap[jstruct].true_pred = true;
            status = mxGetString(tmp, predMap[jstruct].str, buflen);
            status = mxGetString(tmp, miscell->predMap[jstruct].str, buflen);   
                                    
            /* Get range*/
            tmp = mxGetField(prhs[1], jstruct, "range");
            if(tmp != NULL)
            {
                ndim = mxGetNumberOfDimensions(tmp);
                dims = mxGetDimensions(tmp);
                predMap[jstruct].Range = mxGetPr(tmp);
                miscell->predMap[jstruct].Range = mxGetPr(tmp);
            }

            tmp = mxGetField(prhs[1], jstruct, "Normalized");
            if (tmp != NULL)
            {
                miscell->predMap[jstruct].Normalized = mxGetScalar(tmp)>0.5;
                predMap[jstruct].Normalized = miscell->predMap[jstruct].Normalized;
                if (miscell->predMap[jstruct].Normalized)
                {
                    tmp = mxGetField(prhs[1], jstruct, "NormBounds");
                    if (tmp == NULL)
                    {
                        mexPrintf("%s%s \n", "Predicate: ", miscell->predMap[jstruct].str);
                        mexErrMsgTxt("mx_stpl_monitor: Above predicate: 'NormBounds' is empty/undefined when 'Normalized' is set to true!");
                    }
                    ndim = mxGetNumberOfDimensions(tmp);
                    dims = mxGetDimensions(tmp);
                    if (!(ndim == 2 && ((dims[0] == 1 && dims[1] == 1) || (dims[0] == 1 && dims[1] == 2) || (dims[0] == 2 && dims[1] == 1))))
                    {
                        mexPrintf("%s%s \n", "Predicate: ", miscell->predMap[jstruct].str);
                        mexErrMsgTxt("mx_stpl_monitor: Above predicate: 'NormBounds' should be a 1D or 2D vector!");
                    }
                    if (nrhs>4 && !((dims[0] == 1 && dims[1] == 2) || (dims[0] == 2 && dims[1] == 1)))
                    {
                        mexPrintf("%s%s \n", "Predicate: ", miscell->predMap[jstruct].str);
                        mexErrMsgTxt("mx_stpl_monitor: Above predicate: 'NormBounds' should be a 2D vector!");
                    }
                    predMap[jstruct].NormBounds = mxGetPr(tmp);
                    miscell->predMap[jstruct].NormBounds = mxGetPr(tmp);
                }
                else
                {
                    miscell->predMap[jstruct].Normalized = false;
                    predMap[jstruct].Normalized = false;
                    miscell->predMap[jstruct].NormBounds = NULL;
                    predMap[jstruct].NormBounds = NULL;
                }

            }
            /* Get projection */         
            tmp = mxGetField(prhs[1], jstruct, "proj");
            if(tmp == NULL)
            { 
                predMap[jstruct].set.nproj=0;
            }
            else
            {
                ndimP = mxGetNumberOfDimensions(tmp);
                if (ndimP>2)
                {
                    mexPrintf("%s%s \n", "Predicate: ", predMap[jstruct].str);
                    mexErrMsgTxt("mx_stpl_monitor: Above predicate: The projection map is not in proper form!"); 
                }
                dimsP = mxGetDimensions(tmp);
                if(dimsP[0]!=1)
                {
                    mexPrintf("%s%s \n", "Predicate: ", predMap[jstruct].str);
                    mexErrMsgTxt("mx_stpl_monitor: Above predicate: The projection map must be a row vector !"); 
                }
                if(dimsP[1]>miscell->dp_taliro_param.SysDim)
                {
                    mexPrintf("%s%s \n", "Predicate: ", predMap[jstruct].str);
                    mexErrMsgTxt("mx_stpl_monitor: Above predicate: The projection map must have size less than or equal to the system dimentions !"); 
                }
                predMap[jstruct].set.nproj=dimsP[1];
                predMap[jstruct].set.proj=(int *)emalloc(predMap[jstruct].set.nproj*sizeof(int));
                tempI=0;
                for (iii=0; iii<predMap[jstruct].set.nproj; iii++)
                {
                    predMap[jstruct].set.proj[iii] = (int)(mxGetPr(tmp))[iii];
                    if(predMap[jstruct].set.proj[iii]>miscell->dp_taliro_param.SysDim)
                    {
                        mexPrintf("%s%s \n", "Predicate: ", predMap[jstruct].str);
                        mexErrMsgTxt("mx_stpl_monitor: Above predicate: The projection index must be less than or equal to the system dimentions !"); 
                    }
                    if(predMap[jstruct].set.proj[iii]<=tempI)
                    {
                        mexPrintf("%s%s \n", "Predicate: ", predMap[jstruct].str);
                        mexErrMsgTxt("mx_stpl_monitor: Above predicate: The projection index must be incremental !"); 
                    }
                    else
                    {
                        tempI=predMap[jstruct].set.proj[iii];
                    }
                }
            }

            /* Get set */
            tmp = mxGetField(prhs[1], jstruct, "A");
            /* If A is empty, then we should have an interval */
            if(tmp == NULL)
            { 
                tmp = mxGetField(prhs[1], jstruct, "Int");
                if(tmp == NULL) {
                    mexPrintf("%s%s \n", "Predicate: ", predMap[jstruct].str);
                    mexErrMsgTxt("mx_stpl_monitor: Above predicate: Both fields 'A' and 'Int' do not exist!"); 
                }
            }
            else if(mxIsEmpty(tmp))
            { 
                predMap[jstruct].set.isSetRn = true;
                predMap[jstruct].set.ncon = 0;
            }
            else
            {
                predMap[jstruct].set.isSetRn = false;
                /* get A */
                ndimA = mxGetNumberOfDimensions(tmp);
                if (ndimA>2)
                {
                    mexPrintf("%s%s \n", "Predicate: ", predMap[jstruct].str);
                    mexErrMsgTxt("mx_stpl_monitor: Above predicate: The set constraints are not in proper form!"); 
                }
                dimsA = mxGetDimensions(tmp);
                /* for projection is updated */
                if ((miscell->dp_taliro_param.SysDim != dimsA[1]&&predMap[jstruct].set.nproj==0)||(predMap[jstruct].set.nproj!=dimsA[1]&&predMap[jstruct].set.nproj>0))
                {
                    mexPrintf("%s%s \n", "Predicate: ", predMap[jstruct].str);
                    mexErrMsgTxt("mx_stpl_monitor: Above predicate: The dimensions of the set constraints and the state trace do not match!"); 
                }
                predMap[jstruct].set.ncon = dimsA[0]; /* the number of constraints */
                if (predMap[jstruct].set.ncon>2 && miscell->dp_taliro_param.SysDim==1)
                {
                    mexPrintf("%s%s \n", "Predicate: ", predMap[jstruct].str);
                    mexErrMsgTxt("mx_stpl_monitor: Above predicate: For 1D signals only up to two constraints per predicate are allowed!\n More than two are redundant!"); 
                }
                /* for projection is added */
                predMap[jstruct].set.A = (double **)emalloc(predMap[jstruct].set.ncon*sizeof(double*));
                if(predMap[jstruct].set.nproj==0)
                {
                    for (iii=0; iii<predMap[jstruct].set.ncon; iii++)
                    {
                        predMap[jstruct].set.A[iii] = (double *)emalloc(miscell->dp_taliro_param.SysDim*sizeof(double));
                        for (jjj=0; jjj<miscell->dp_taliro_param.SysDim; jjj++)
                            predMap[jstruct].set.A[iii][jjj] = (mxGetPr(tmp))[iii+jjj*predMap[jstruct].set.ncon];
                    }
                }
                else
                {
                    for (iii=0; iii<predMap[jstruct].set.ncon; iii++)
                    {
                        predMap[jstruct].set.A[iii] = (double *)emalloc(predMap[jstruct].set.nproj*sizeof(double));
                        for (jjj=0; jjj<predMap[jstruct].set.nproj; jjj++)
                            predMap[jstruct].set.A[iii][jjj] = (mxGetPr(tmp))[iii+jjj*predMap[jstruct].set.ncon];
                    }
                }
                /* for projection */

            
                /* get b */
                tmp = mxGetField(prhs[1], jstruct, "b");
                if(tmp == NULL) 
                { 
                    mexPrintf("%s%s\n", "Predicate: ", predMap[jstruct].str);
                    mexErrMsgTxt("mx_stpl_monitor: Above predicate: Field 'b' is empty!"); 
                }
                ndimb = mxGetNumberOfDimensions(tmp);
                if (ndimb>2)
                {
                    mexPrintf("%s%s\n", "Predicate: ", predMap[jstruct].str);
                    mexErrMsgTxt("mx_stpl_monitor: Above predicate: The set constraints are not in proper form!"); 
                }
                dimsb = mxGetDimensions(tmp);
                if (predMap[jstruct].set.ncon != dimsb[0])
                {
                    mexPrintf("%s%s\n", "Predicate: ", predMap[jstruct].str);
                    mexErrMsgTxt("mx_stpl_monitor: Above predicate: The number of constraints between A and b do not match!"); 
                }
                predMap[jstruct].set.b = mxGetPr(tmp);
                if (predMap[jstruct].set.ncon==2 && (miscell->dp_taliro_param.SysDim==1||predMap[jstruct].set.nproj==1))
                {
                    if ((predMap[jstruct].set.A[0][0]>0 && predMap[jstruct].set.A[1][0]>0) || 
                        (predMap[jstruct].set.A[0][0]<0 && predMap[jstruct].set.A[1][0]<0))
                    {
                        mexPrintf("%s%s\n", "Predicate: ", predMap[jstruct].str);
                        mexErrMsgTxt("mx_stpl_monitor: Above predicate: The set has redundant constraints! Please remove redundant constraints."); 
                    }
                    if (!((predMap[jstruct].set.A[0][0]<0 && (predMap[jstruct].set.b[0]/predMap[jstruct].set.A[0][0]<=predMap[jstruct].set.b[1]/predMap[jstruct].set.A[1][0])) || 
                          (predMap[jstruct].set.A[1][0]<0 && (predMap[jstruct].set.b[1]/predMap[jstruct].set.A[1][0]<=predMap[jstruct].set.b[0]/predMap[jstruct].set.A[0][0]))))
                    {
                        mexPrintf("%s%s\n", "Predicate: ", predMap[jstruct].str);
                        mexErrMsgTxt("mx_stpl_monitor: Above predicate: The set is empty! Please modify the constraints."); 
                    }
                }
            }            
            /* get control locations */
            if (nrhs>4)
            {
                tmp = mxGetField(prhs[1], jstruct, "loc");
                if (is_Multi_HA == 1) {
                    predMap[jstruct].set.nloc = 0;
                    predMap[jstruct].set.locNCLG = (double **)emalloc((miscell->dp_taliro_param.nCLG) * sizeof(double *));
                    predMap[jstruct].set.nlocNCLG = (int *)emalloc((miscell->dp_taliro_param.nCLG) * sizeof(int));
                    if (tmp == NULL) {
                        for (objs = 0;objs<miscell->dp_taliro_param.nCLG;objs++) {
                            predMap[jstruct].set.nlocNCLG[objs] = 0;
                            predMap[jstruct].set.locNCLG[objs] = NULL;
                        }
                    }
                    else
                        for (objs = 0;objs<miscell->dp_taliro_param.nCLG;objs++) {
                            tmp1 = mxGetCell(tmp, objs);
                            if (tmp1 == NULL)
                            {
                                predMap[jstruct].set.nlocNCLG[objs] = 0;
                                predMap[jstruct].set.locNCLG[objs] = NULL;
                            }
                            else
                            {
                                NCells = mxGetNumberOfElements(tmp1);
                                ndim = mxGetNumberOfDimensions(tmp1);
                                if (ndim>2)
                                {
                                    mexPrintf("%s%s\n", "Predicate: ", predMap[jstruct].str);
                                    mexErrMsgTxt("Above predicate: The control location vector is not in proper form!");
                                }
                                dims = mxGetDimensions(tmp1);
                                if (dims[0]>1)
                                {
                                    mexPrintf("%s%s\n", "Predicate: ", predMap[jstruct].str);
                                    mexErrMsgTxt("Above predicate: The control location vector must be row vector!");
                                }
                                predMap[jstruct].set.nlocNCLG[objs] = dims[1];
                                predMap[jstruct].set.locNCLG[objs] = mxGetPr(tmp1);
                            }
                        }
                }
                else {
                    NCells = mxGetNumberOfElements(tmp);
                    if(tmp == NULL) 
                    { 
                        mexPrintf("%s%s\n", "Predicate: ", predMap[jstruct].str);
                        mexErrMsgTxt("mx_stpl_monitor: Above predicate: Field 'loc' is empty!"); 
                    }
                    ndim = mxGetNumberOfDimensions(tmp);
                    if (ndim>2)
                    {
                        mexPrintf("%s%s\n", "Predicate: ", predMap[jstruct].str);
                        mexErrMsgTxt("mx_stpl_monitor: Above predicate: The control location vector is not in proper form!"); 
                    }
                    dims = mxGetDimensions(tmp);
                    if (dims[0]>1)
                    {
                        mexPrintf("%s%s\n", "Predicate: ", predMap[jstruct].str);
                        mexErrMsgTxt("mx_stpl_monitor: Above predicate: The control location vector must be row vector!"); 
                    }
                    predMap[jstruct].set.nloc = dims[1];
                    predMap[jstruct].set.loc = mxGetPr(tmp);
                }
            }
        }
    }

    
    
    //moved here to fill in the type_map first before
    //processing the predicates
    node = tl_parse(cnt, hasuform, uform, miscell, tl_yychar);//<<<<<<<<<<<<<
    
                                                              
    //in order to update the unknown variables to VAR_ID
    //we have to rerun this after parsing the formula (i.e., tl_parse) and
    //having id_to_rank_map updated -> find_in_map(&id_to_rank_map,...)
    for (jstruct = 0; jstruct < NElems; jstruct++)
        if (predMap[jstruct].fn_expr != NULL)
        count_num_unique_vars_in_pred_expression(predMap[jstruct].fn_expr);
    
    //update the predMap with predicate expressions from the formula
    //tl_parse updates miscell with them
    for (int i = 0; i < miscell->extra_pred_from_formula; i++) {
        miscell->extra_pmap[i].fn_expr->num_unique_vars = 
            count_num_unique_vars_in_pred_expression(miscell->extra_pmap[i].fn_expr);
        predMap[i + NElems] = miscell->extra_pmap[i];
        miscell->predMap[i + NElems] = miscell->extra_pmap[i];
        if (VERBOSE_MODE)
        {
            mexPrintf("\nAdded predicate expression:\n%s ::= ", predMap[i + NElems].str);
            print_fn_def(predMap[i + NElems].fn_expr->left_fn);
            print_opr(predMap[i + NElems].fn_expr->opr);
            print_fn_def(predMap[i + NElems].fn_expr->right_fn);
            mexPrintf("\n");
            mexPrintf("number of unique variables: %d\n", predMap[jstruct].fn_expr->num_unique_vars);
        }

    }

    if (VERBOSE_MODE)
        mexPrintf("\n");

    NElems += miscell->extra_pred_from_formula;
    miscell->dp_taliro_param.nPred = NElems;
    //miscell->dp_taliro_param.true_nPred = NElems;

    miscell->tl_out = stdout; 

    /* set up some variables wrt to timing constraints */
    miscell->zero2inf.l_closed = 1;
    miscell->zero2inf.u_closed = 0;
    miscell->emptyInter.l_closed = 0;
    miscell->emptyInter.u_closed = 0;
    if (miscell->dp_taliro_param.ConOnSamples)
    {    
        miscell->zero.numi.inf = 0;
        miscell->zero.numi.i_num = 0;
        miscell->inf.numi.inf = 1;
        miscell->inf.numi.i_num = 0;
        miscell->zero2inf.lbd = miscell->zero; 
        miscell->zero2inf.ubd = miscell->inf;
        miscell->emptyInter.lbd = miscell->zero;
        miscell->emptyInter.ubd = miscell->zero;
    }
    else
    {    
        miscell->zero.numf.inf = 0;
        miscell->zero.numf.f_num = 0.0;
        miscell->inf.numf.inf = 1;
        miscell->inf.numf.f_num = 0.0;
        miscell->zero2inf.lbd = miscell->zero;
        miscell->zero2inf.ubd = miscell->inf;
        miscell->emptyInter.lbd = miscell->zero;
        miscell->emptyInter.ubd = miscell->zero;
    }

    
    ////node = tl_parse(cnt,hasuform,uform, miscell, tl_yychar);//<<<<<<<<<<<<<
    temp = 0;
    cnt = &temp;
    int num_qfr = 0;
    int* qfr_ptr = &num_qfr;

    //counts the number of freeze time/frame and quantifiers in the entire formula
    countVar(cnt, node, NULL, 0, qfr_ptr);

    if((*cnt) > 0 || (*qfr_ptr) > 0){
        if((*cnt) > 0)
            miscell->dp_taliro_param.TPTL = 1;
        if(stql_compliance && (*qfr_ptr) > 0)
            miscell->dp_taliro_param.STQL = 1;
        miscell->dp_taliro_param.LTL=0;
        /*mexPrintf("\nTPTL\n");*/
    }
    else{
        miscell->dp_taliro_param.TPTL=0;
        miscell->dp_taliro_param.LTL=1;
        /*mexPrintf("\nLTL\n");*/
    }
        
    if (miscell->dp_taliro_param.LTL==0 && nrhs<4)
        mexErrMsgTxt("mx_stpl_monitor: The formula is in MTL, but there are no timestamps!"); 


    plhs[0] = DP(node, predMap, XTrace, TStamps, LTrace, 
                    &distData, 
                    &(miscell->dp_taliro_param), 
                    miscell);//<<<<<<<<<<<<<<<<

    if (plhs[0] == NULL)
        return;
        release_fn_expr_from_nodes(node);
    
    //@todo: release other allocated memories 

    for (iii=0; iii<miscell->dp_taliro_param.true_nPred; iii++)
    {
        tl_clearlookup(predMap[iii].str, miscell);
        for (jjj=0;jjj<predMap[iii].set.ncon;jjj++)
            efree(predMap[iii].set.A[jjj]);
        efree(predMap[iii].str);
        efree(predMap[iii].set.A);

    }
    for (iii=0; iii< miscell->dp_taliro_param.true_nPred; iii++)
    {
        if(miscell->predMap[iii].str != NULL)
            efree(miscell->predMap[iii].str);
    }    
    efree(miscell->pList.pindex);
    efree(miscell->predMap);
    efree(predMap);
    /* for projection and multiple H.A.s is updated */
    if (nrhs>7)
    {
        if(is_Multi_HA==0){
            for (iii=0; iii<miscell->dp_taliro_param.tnLoc; iii++)
            {
                for (jjj=0; jjj<distData.AdjLNell[iii]; jjj++)
                {
                    for (kk=0; kk<distData.GuardMap[iii][jjj].nset; kk++)
                    {
                        for (i1=0; i1<distData.GuardMap[iii][jjj].ncon[kk]; i1++){
                            efree(distData.GuardMap[iii][jjj].A[kk][i1]);
                        }
                        efree(distData.GuardMap[iii][jjj].A[kk]);
                    }
                    efree(distData.GuardMap[iii][jjj].A);
                    efree(distData.GuardMap[iii][jjj].b);
                    efree(distData.GuardMap[iii][jjj].ncon);                
                }
                efree(distData.GuardMap[iii]);
            }
            efree(distData.GuardMap);
            efree(distData.AdjLNell);
            efree(distData.AdjL);
        }
    }
    efree(miscell);
    efree(tl_yychar);
    
}

//new STPL proxy function
mxArray* nonmexFunction(char* stpl_formula, int size_formula,
    int num_predicates, char* pred_str[], char* pred_expr[], double* input_signal,
    int num_rows, int num_columns, double* TStamps)
{
    run_from_matlab = false;

    /* Variables needed to process the input */
    int status;// , pstatus;
    mwSize buflen;// , pbuflen;
    size_t NElems;
    mwIndex jstruct, iii, jjj;
    mxArray* tmp;
    /* Variables needed for monitor */
    Node* node;
    double* XTrace = NULL, * LTrace = NULL;
    DistCompData distData;
    PMap* predMap;
    int ll;

    static char    uform[BUFF_LEN];
    static size_t hasuform = 0;
    static int* cnt;
    int temp = 0;


    Miscellaneous* miscell = (Miscellaneous*)emalloc(sizeof(Miscellaneous));
    int* tl_yychar = (int*)emalloc(sizeof(int));
    miscell->dp_taliro_param.LTL = 1;
    miscell->dp_taliro_param.ConOnSamples = 0;
    miscell->dp_taliro_param.SysDim = 0;
    miscell->dp_taliro_param.nSamp = 0;
    miscell->dp_taliro_param.nPred = 0;
    miscell->dp_taliro_param.true_nPred = 0;
    miscell->dp_taliro_param.tnLoc = 0;
    miscell->dp_taliro_param.nInp = 0;
    miscell->dp_taliro_param.nCLG = 1;
    miscell->tl_errs = 0;
    miscell->type_temp = 0;

    /* Reset cnt to 0:
        cnt is the counter that points to the next symbol in the formula
        to be processed. This is a static variable and it retains its
        value between Matlab calls to mx_stpl_monitor. */
    cnt = &temp;

    /* Other initializations */
    miscell->dp_taliro_param.nInp = num_predicates;
    bool has_predicate_map = false;


    mxArray* plhs[1];
    plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);

    /* Process inputs */
    /* Get the formula */
    buflen = (size_formula +1) * sizeof(char);
    if (buflen >= BUFF_LEN)
    {
        mexPrintf("%s%d%s\n", "The formula must be less than ", BUFF_LEN, " characters.");
        mexErrMsgTxt("mx_stpl_monitor: Formula too long.");
    }
    safe_strcpy(uform, buflen, stpl_formula);
    hasuform = strlen(uform);
    for (iii = 0; iii < hasuform; iii++)
    {
        if (uform[iii] == '\t' || uform[iii] == '\"' || uform[iii] == '\n')
            uform[iii] = ' ';
    }


    miscell->dp_taliro_param.nSamp = num_rows;
    miscell->dp_taliro_param.SysDim = num_columns;
    XTrace = input_signal;

        //check if the input signal is in a right format w.r.t STQL
    bool stql_compliance = true;
    if (num_columns < STQL_SIGNAL_DIM_SIZE)
        stql_compliance = false;
    else {
        int j = num_rows;
        for (int i = 0; i < num_rows - 1; i++) {
            if (XTrace[i] != (int)fabs(XTrace[i]) ||//frame numbers are integer i.e., 0,1,2...,n
                XTrace[i + 1] < XTrace[i] ||//frame numbers are monotically increasing
                XTrace[j+i] != fabs(XTrace[j+i]) ||//time stamps are positive
                XTrace[j+i + 1] < XTrace[j+i] //time stamps are monotically increasing
                ) {
                stql_compliance = false;
                mexPrintf("f=%f t=%f\n", XTrace[i], XTrace[i + 1]);
                break;
            }
            //j++;
        }
    }
    if (!stql_compliance)
        if (SHOW_WARNINGS)
            mexPrintf("WARNING: the input signal is not in compliance with STPL!\n");
    if (stql_compliance) {
                miscell->dp_taliro_param.nFrame = (int)(XTrace[num_rows - 1] - XTrace[0] + 1);
    }
    else {
        miscell->dp_taliro_param.nFrame = 0;
    }


    /* Get predicate map*/
    NElems = num_predicates;
    miscell->dp_taliro_param.nPred = NElems;
    miscell->dp_taliro_param.true_nPred = NElems;
    if (NElems == 0 && has_predicate_map)
        mexErrMsgTxt("mx_stpl_monitor: the predicate map is empty!");
    predMap = (PMap*)emalloc((NElems + MAX_PREDICATE_EXPRESSION_IN_FORMULA) * sizeof(PMap));
    miscell->predMap = (PMap*)emalloc((NElems + MAX_PREDICATE_EXPRESSION_IN_FORMULA) * sizeof(PMap));

    miscell->pList.pindex = (int*)emalloc((NElems + MAX_PREDICATE_EXPRESSION_IN_FORMULA) * sizeof(int));


    /* initial predicate list*/
    for (ll = 0; ll < NElems; ll++)
    {
        miscell->pList.pindex[ll] = -1;
    }

    for (jstruct = 0; jstruct < NElems; jstruct++)
    {
        predMap[jstruct].is_expression = true;

                /* Get name of the predicate */
        buflen = (strlen(pred_str[jstruct]) +1) * sizeof(char) ;
        predMap[jstruct].str = (char*)emalloc((strlen(pred_str[jstruct]) + 1) * sizeof(char));
        miscell->predMap[jstruct].str = (char*)emalloc((strlen(pred_str[jstruct]) + 1) * sizeof(char));
        predMap[jstruct].set.idx = (int)jstruct;
        predMap[jstruct].true_pred = true;
        safe_strcpy(predMap[jstruct].str, buflen, pred_str[jstruct]);
        safe_strcpy(miscell->predMap[jstruct].str, buflen, pred_str[jstruct]);

        /* Get function expression*/
        buflen = (strlen(pred_expr[jstruct]) +1) * sizeof(char);
        predMap[jstruct].expr = (char*)emalloc((strlen(pred_expr[jstruct]) + 1) * sizeof(char));
        miscell->predMap[jstruct].expr = (char*)emalloc((strlen(pred_expr[jstruct]) + 1) * sizeof(char));
        predMap[jstruct].is_expression = true;
        miscell->predMap[jstruct].is_expression = true;
        safe_strcpy(predMap[jstruct].expr, buflen, pred_expr[jstruct]);
        safe_strcpy(miscell->predMap[jstruct].expr, buflen, pred_expr[jstruct]);
        predMap[jstruct].fn_expr = parse_fn_predicate_expression(predMap[jstruct].expr);//<<<<<<<<<
        if (VERBOSE_MODE)
        {
            mexPrintf("\nPredicate expression:\n%s ::= ", predMap[jstruct].str);
            print_fn_def(predMap[jstruct].fn_expr->left_fn);
            print_opr(predMap[jstruct].fn_expr->opr);
            print_fn_def(predMap[jstruct].fn_expr->right_fn);
            mexPrintf("\n");
            mexPrintf("number of unique variables: %d\n", predMap[jstruct].fn_expr->num_unique_vars);
        }

    }


    
    //moved here to fill in the type_map first before
    //processing the predicates
    node = tl_parse(cnt, hasuform, uform, miscell, tl_yychar);//<<<<<<<<<<<<<


    //in order to update the unknown variables to VAR_ID
    //we have to rerun this after parsing the formula (i.e., tl_parse) and
    //having id_to_rank_map updated -> find_in_map(&id_to_rank_map,...)
    for (jstruct = 0; jstruct < NElems; jstruct++)
        if (predMap[jstruct].fn_expr != NULL)
            count_num_unique_vars_in_pred_expression(predMap[jstruct].fn_expr);

    //update the predMap with predicate expressions from the formula
    //tl_parse updates miscell with them
    for (int i = 0; i < miscell->extra_pred_from_formula; i++) {
        miscell->extra_pmap[i].fn_expr->num_unique_vars =
            count_num_unique_vars_in_pred_expression(miscell->extra_pmap[i].fn_expr);
        predMap[i + NElems] = miscell->extra_pmap[i];
        miscell->predMap[i + NElems] = miscell->extra_pmap[i];
        if (VERBOSE_MODE)
        {
            mexPrintf("\nAdded predicate expression:\n%s ::= ", predMap[i + NElems].str);
            print_fn_def(predMap[i + NElems].fn_expr->left_fn);
            print_opr(predMap[i + NElems].fn_expr->opr);
            print_fn_def(predMap[i + NElems].fn_expr->right_fn);
            mexPrintf("\n");
            mexPrintf("number of unique variables: %d\n", predMap[jstruct].fn_expr->num_unique_vars);
        }

    }

    if (VERBOSE_MODE)
        mexPrintf("\n");

    NElems += miscell->extra_pred_from_formula;
    miscell->dp_taliro_param.nPred = NElems;

    miscell->tl_out = stdout;

    /* set up some variables wrt to timing constraints */
    miscell->zero2inf.l_closed = 1;
    miscell->zero2inf.u_closed = 0;
    miscell->emptyInter.l_closed = 0;
    miscell->emptyInter.u_closed = 0;
    if (miscell->dp_taliro_param.ConOnSamples)
    {
        miscell->zero.numi.inf = 0;
        miscell->zero.numi.i_num = 0;
        miscell->inf.numi.inf = 1;
        miscell->inf.numi.i_num = 0;
        miscell->zero2inf.lbd = miscell->zero;
        miscell->zero2inf.ubd = miscell->inf;
        miscell->emptyInter.lbd = miscell->zero;
        miscell->emptyInter.ubd = miscell->zero;
    }
    else
    {
        miscell->zero.numf.inf = 0;
        miscell->zero.numf.f_num = 0.0;
        miscell->inf.numf.inf = 1;
        miscell->inf.numf.f_num = 0.0;
        miscell->zero2inf.lbd = miscell->zero;
        miscell->zero2inf.ubd = miscell->inf;
        miscell->emptyInter.lbd = miscell->zero;
        miscell->emptyInter.ubd = miscell->zero;
    }

    
    ////node = tl_parse(cnt,hasuform,uform, miscell, tl_yychar);//<<<<<<<<<<<<<
    temp = 0;
    cnt = &temp;
    int num_qfr = 0;
    int* qfr_ptr = &num_qfr;

    //counts the number of freeze time/frame and quantifiers in the entire formula
    countVar(cnt, node, NULL, 0, qfr_ptr);

    if ((*cnt) > 0 || (*qfr_ptr) > 0) {
        if ((*cnt) > 0)
            miscell->dp_taliro_param.TPTL = 1;
        if (stql_compliance && (*qfr_ptr) > 0)
            miscell->dp_taliro_param.STQL = 1;
        miscell->dp_taliro_param.LTL = 0;
        /*mexPrintf("\nTPTL\n");*/
    }
    else {
        miscell->dp_taliro_param.TPTL = 0;
        miscell->dp_taliro_param.LTL = 1;
        /*mexPrintf("\nLTL\n");*/
    }

    plhs[0] = DP(node, predMap, XTrace, TStamps, LTrace,
        &distData,
        &(miscell->dp_taliro_param),
        miscell);//<<<<<<<<<<<<<<<<

    if (plhs[0] == NULL) {
        return NULL;
    }

    release_fn_expr_from_nodes(node);

    //@todo: release other allocated memories 

    for (iii = 0; iii < miscell->dp_taliro_param.true_nPred; iii++)
    {
        tl_clearlookup(predMap[iii].str, miscell);
        for (jjj = 0; jjj < predMap[iii].set.ncon; jjj++)
            efree(predMap[iii].set.A[jjj]);
        efree(predMap[iii].str);
        efree(predMap[iii].set.A);

    }
    for (iii = 0; iii < miscell->dp_taliro_param.true_nPred; iii++)
    {
        if (miscell->predMap[iii].str != NULL)
            efree(miscell->predMap[iii].str);
    }
    efree(miscell->pList.pindex);
    efree(miscell->predMap);
    efree(predMap);
    /* for projection and multiple H.A.s is updated */
    efree(miscell);
    efree(tl_yychar);

    return plhs[0];
}

bool read_global_variables_from_config_file(char* input_file_name, bool verbose_from_command) {

    bool is_open = open_config_file(input_file_name, verbose_from_command);

    if (!is_open)
        return false;

    char* cfg_value;
    cfg_value = (char*)cfg_get_value("DEBUG_MODE");
    if (strcmp(cfg_value, "true") == 0) {
        DEBUG_MODE = true;
    }
    free(cfg_value);
    cfg_value = (char*)cfg_get_value("VERBOSE_MODE");
    if (strcmp(cfg_value, "true") == 0) {
        VERBOSE_MODE = true;
    }
    else if (verbose_from_command)
        VERBOSE_MODE = true;
    free(cfg_value);
    cfg_value = (char*)cfg_get_value("SHOW_EXE_STAT");
    if (strcmp(cfg_value, "true") == 0) {
        SHOW_EXE_STAT = true;
    }
    free(cfg_value);
    cfg_value = (char*)cfg_get_value("SANITY_CHECK");
    if (strcmp(cfg_value, "true") == 0) {
        SANITY_CHECK = true;
    }
    free(cfg_value);

    if (VERBOSE_MODE)
        printf("DEBUG_MODE is activated.\n");
    if (VERBOSE_MODE)
        printf("VERBOSE_MODE is activated.\n");
    if (VERBOSE_MODE)
        printf("SHOW_EXE_STAT is activated.\n");
    if (VERBOSE_MODE)
        printf("SANITY_CHECK is activated.\n");

    cfg_value = (char*)cfg_get_value("BOOLEAN_ROB");
    if (strcmp(cfg_value, "true") == 0) {
        if (VERBOSE_MODE)
            printf("BOOLEAN_ROB is activated.\n");
        BOOLEAN_ROB = true;
    }
    free(cfg_value);
    /*cfg_value = (char*)cfg_get_value("RELEASE_DP_TABLE_MEMORY");
    if (strcmp(cfg_value, "true") == 0) {
        if (VERBOSE_MODE)
            printf("RELEASE_DP_TABLE_MEMORY is activated.\n");
        RELEASE_DP_TABLE_MEMORY = true;
    }
    free(cfg_value);
    cfg_value = (char*)cfg_get_value("RELEASE_SPATIAL_MEMORY");
    if (strcmp(cfg_value, "true") == 0) {
        if (VERBOSE_MODE)
            printf("RELEASE_SPATIAL_MEMORY is activated.\n");
        RELEASE_SPATIAL_MEMORY = true;
    }
    free(cfg_value);*/
    cfg_value = (char*)cfg_get_value("OPTIMIZE_TRJ_BASED_ON_FORMULA");
    if (strcmp(cfg_value, "true") == 0) {
        if (VERBOSE_MODE)
            printf("OPTIMIZE_TRJ_BASED_ON_FORMULA is activated.\n");
        OPTIMIZE_TRJ_BASED_ON_FORMULA = true;
    }
    free(cfg_value);
    cfg_value = (char*)cfg_get_value("STPL_SIGNAL_DIM_SIZE");
    if (strlen(cfg_value) > 0) {
        if (VERBOSE_MODE)
            printf("STPL_SIGNAL_DIM_SIZE = %s\n", cfg_value);
        STQL_SIGNAL_DIM_SIZE = atoi(cfg_value);
    }
    free(cfg_value);
    cfg_value = (char*)cfg_get_value("MAX_STPL_FORMULA_SIZE");
    if (strlen(cfg_value) > 0) {
        if (VERBOSE_MODE)
            printf("MAX_STPL_FORMULA_SIZE = %s\n", cfg_value);
        MAX_STQL_FORMULA_SIZE = atoi(cfg_value);
    }
    free(cfg_value);
    cfg_value = (char*)cfg_get_value("MAX_MAP_DATA_SIZE");
    if (strlen(cfg_value) > 0) {
        if (VERBOSE_MODE)
            printf("MAX_MAP_DATA_SIZE = %s\n", cfg_value);
        MAX_MAP_DATA_SIZE = atoi(cfg_value);
    }
    free(cfg_value);
    cfg_value = (char*)cfg_get_value("UX_START");
    if (strlen(cfg_value) > 0) {
        if (VERBOSE_MODE)
            printf("UX_START = %s\n", cfg_value);
        UX_START = atoi(cfg_value);
    }
    free(cfg_value);
    cfg_value = (char*)cfg_get_value("UY_START");
    if (strlen(cfg_value) > 0) {
        if (VERBOSE_MODE)
            printf("UY_START = %s\n", cfg_value);
        UY_START = atoi(cfg_value);
    }
    free(cfg_value);
    cfg_value = (char*)cfg_get_value("U_WIDTH");
    if (strlen(cfg_value) > 0) {
        if (VERBOSE_MODE)
            printf("U_WIDTH = %s\n", cfg_value);
        U_WIDTH = atoi(cfg_value);
    }
    free(cfg_value);
    cfg_value = (char*)cfg_get_value("U_HEIGHT");
    if (strlen(cfg_value) > 0) {
        if (VERBOSE_MODE)
            printf("U_HEIGHT = %s\n", cfg_value);
        U_HEIGHT = atoi(cfg_value);
    }
    free(cfg_value);

    if (UX_START >= 0)
        _UNIVERSE[0] = UX_START;
    if (UY_START >= 0)
        _UNIVERSE[2] = UY_START;
    if (U_WIDTH >= 0)
        _UNIVERSE[1] = _UNIVERSE[0] + U_WIDTH;
    if (U_HEIGHT >= 0)
        _UNIVERSE[3] = _UNIVERSE[2] + U_HEIGHT;
}

bool regression_tests() {
/***************************************************/
//activate this section for testing spatial operators
/*{
    init_garbage_collector();
    run_spatial_test1();
    run_spatial_test2();
    run_spatial_test3();
    release_garbage();
    mexErrMsgTxt("TEST ENDED!");
}*/
/***************************************************/
    return true;
}
