flist = ls('label_02');
for i=1:length(flist)
    fname = flist(i,:);
    %signal = kitti_occlusion_extractor('data_tracking_label_2-training-label_02-0008.txt', 20, true);
    if endsWith(fname,'.txt')
        %fname = '0013.txt';
        disp(['Reading ', fname])
        signal = kitti_occlusion_extractor(['./label_02/', fname], 20, true);
        outfile = ['./out/kitti-occlusion-', fname];
        writematrix(signal, outfile, 'Delimiter','tab');
        %save(outfile, 'signal','-ascii');
    end
end